# BackendSpring

[![CircleCI](https://circleci.com/gh/InYourHead/BackendBootSpringREST.svg?style=svg&circle-token=d1de1dfbf723d0e7336e5ace9a173fde85d4e8f6)](https://circleci.com/gh/InYourHead/BackendBootSpringREST)

Dokumentacja pod adresem:
    http://matumk.iboss.pl/2.0/index.html

Projekt zawiera wbudowaną bazę danych, oraz wbudowanego Tomcata.
Aby korzystać z tomcata, należy:

1. wygenerować klucz komendą:
    
    keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650

2. Podać hasło: MyFuckingGreatKeyStore

3. Następnie skopiować klucz keystore.p12 do katalogu src/main/resource i zastąpić istniejący klucz.

4. Przechodzimy do BackendSpring i uruchamiamy aplikację komendą:

    ./start
