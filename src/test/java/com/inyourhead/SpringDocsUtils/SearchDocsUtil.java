package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * Created by aleksander on 29.12.16.
 */
public class SearchDocsUtil {

    public static ResultHandler getStoreResultAsLoggedInUser() {
        return document("search/getStoreResultAsLoggedInUser", preprocessResponse(prettyPrint()));

    }

    public static ResultHandler getProductResultAsLoggedInUser() {
        return document("search/getProductResultAsLoggedInUser", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler getStoreResultAsLoggedInUserUsingGET() {
        return document("search/getStoreResultAsLoggedInUserUsingGET", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler getProductResultAsLoggedInUserUsingGET() {
        return document("search/getProductResultAsLoggedInUserUsingGET", preprocessResponse(prettyPrint()));
    }
}
