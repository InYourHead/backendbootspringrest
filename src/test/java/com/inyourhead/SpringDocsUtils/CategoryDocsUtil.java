package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * Created by aleksander on 30.12.16.
 */
public class CategoryDocsUtil {

    public static ResultHandler getCategories() {
        return document("categories", preprocessResponse(prettyPrint())
        );
    }

    public static ResultHandler getProvinces() {
        return document("provinces", preprocessResponse(prettyPrint())
        );
    }

    public static ResultHandler getDefaultProducts() {
        return document("defaultProducts", preprocessResponse(prettyPrint())
        );
    }

    public static ResultHandler createDefaultProduct() {
        return document("createDefaultProduct", preprocessResponse(prettyPrint())
        );
    }

    public static ResultHandler getProductsOfSelectedStore() {
        return document("getProductsOfSelectedStore", preprocessResponse(prettyPrint())
        );
    }
}
