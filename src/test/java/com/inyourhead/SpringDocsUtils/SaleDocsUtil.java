package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;

/**
 * Created by aleksander on 28.12.16.
 */
public class SaleDocsUtil {
    public static ResultHandler getSale() {

        return document("sale/getSale", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }

    public static ResultHandler deleteSale() {

        return document("sale/deleteSale", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }

    public static ResultHandler updateSale() {

        return document("sale/updateSale", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }

    public static ResultHandler createSale() {

        return document("sale/createSale", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }
}
