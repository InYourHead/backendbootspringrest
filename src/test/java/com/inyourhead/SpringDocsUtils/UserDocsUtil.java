package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * Created by aleksander on 08.12.16.
 */
public class UserDocsUtil {
    public static ResultHandler getInfoAboutUser() {
        return document("me/getInfo", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler deleteUserAccount() {

        return document("me/deleteAccount", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler updateUserData() {
        return document("me/updateAccount", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler getUserTokens() {
        return document("me/getTokens", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler deleteUserTokens() {
        return document("me/deleteTokens", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler changePasswordOfLoggedUser() {
        return document("me/updatePassword", preprocessResponse(prettyPrint()));
    }
}
