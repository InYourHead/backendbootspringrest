package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;


/**
 * Created by aleksander on 05.12.16.
 */
public class LoginDocsUtil {
    public static ResultHandler login() {
        return document("login", preprocessResponse(prettyPrint()), requestHeaders(headerWithName("Authorization").description("https://en.wikipedia.org/wiki/Basic_access_authentication"))
        );

    }

    public static ResultHandler logout() {
        return document("logout", preprocessResponse(prettyPrint())
        );
    }
}
