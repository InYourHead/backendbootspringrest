package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;

/**
 * Created by aleksander on 06.12.16.
 */
public class StoreDocsUtil {
    public static ResultHandler getAllStores() {

        return document("stores/getAllStores", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler createNewStore() {
        return document("stores/createStore", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler deleteAllStores() {
        return document("stores/deleteStories", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler getSelectedStore() {
        return document("stores/getSelectedStore", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }

    public static ResultHandler updateStore() {
        return document("stores/updateStore", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }

    public static ResultHandler deleteSelectedStore() {
        return document("stores/deleteSelectedStore", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }
}
