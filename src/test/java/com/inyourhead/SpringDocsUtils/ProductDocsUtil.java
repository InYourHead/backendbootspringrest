package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
/**
 * Created by aleksander on 26.12.16.
 */
public class ProductDocsUtil {
    public static ResultHandler getAllProducts() {
        return document("products/getAllProducts", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }

    public static ResultHandler deleteAllProducts() {
        return document("products/deleteAllProducts", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }

    public static ResultHandler createProduct() {
        return document("products/createProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu")));
    }

    public static ResultHandler getSelectedProduct() {
        return document("products/getSelectedProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }

    public static ResultHandler deleteSelectedProduct() {
        return document("products/deleteSelectedProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }

    public static ResultHandler updateSelectedProduct() {
        return document("products/updateSelectedProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("storeId").description("Id wybranego sklepu"), parameterWithName("productId").description("Id wybranego produktu")));
    }
}
