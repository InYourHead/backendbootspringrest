package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * Created by aleksander on 05.12.16.
 */
public class RegisterDocsUtil {
    public static ResultHandler registerNewUser() {
        return document("register", preprocessResponse(prettyPrint()));
    }

    public static ResultHandler sendResetPasswordEmail() {
        return document("getResetEmail", preprocessResponse(prettyPrint()));
    }
}
