package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * Created by aleksander on 25.12.16.
 */
public class HistoryDocsUtils {
    public static ResultHandler getHistory() {
        return document("history/getAll", preprocessResponse(prettyPrint())
        );
    }

    public static ResultHandler deleteHistory() {
        return document("history/deleteAll", preprocessResponse(prettyPrint()));
    }
}
