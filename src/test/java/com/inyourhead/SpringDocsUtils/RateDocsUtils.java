package com.inyourhead.SpringDocsUtils;

import org.springframework.test.web.servlet.ResultHandler;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;

/**
 * Created by aleksander on 06.04.17.
 */
public class RateDocsUtils {
    public static ResultHandler rateSelectedProduct() {
        return document("rate/rateSelectedProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("productId").description("Id wybranego produktu"))
        );
    }

    public static ResultHandler getUserRateOfSelectedProduct() {
        return document("rate/getUserRateOfSelectedProduct", preprocessResponse(prettyPrint()), pathParameters(parameterWithName("productId").description("Id wybranego produktu")));
    }

}
