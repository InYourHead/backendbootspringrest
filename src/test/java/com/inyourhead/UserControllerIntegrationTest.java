package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.UserDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.NewPasswordDTO;
import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.utils.PasswordGenerator;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.UserController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 08.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, UserController.class})
@Transactional
@ActiveProfiles("test")
@PropertySource(value = "classpath:application.properties")
public class UserControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;

    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;
    @Value("${token.header}")
    private String header;

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldReturnInfoAboutMyself() throws Exception {

        mockMvc.perform(get("/user").header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.login").value("admin"))
                .andDo(UserDocsUtil.getInfoAboutUser());

    }

    @Test
    public void shouldDeleteUserAccount() throws Exception {

        mockMvc.perform(delete("/user").header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.deleteCount").value(1))
                .andDo(UserDocsUtil.deleteUserAccount());

        Assert.assertTrue(repository.findAll().size() == 1);
    }

    @Test
    public void shouldUpdateUserData() throws Exception {

        UzytkownikDTO uzytkownik = new UzytkownikDTO();

        uzytkownik.setEmail("nowy@wp.pl");

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String json = gson.toJson(uzytkownik);
        System.out.println("Przesyłany uzytkownikDTO: " + json);
        mockMvc.perform(patch("/user").header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.email").value("nowy@wp.pl"))
                .andDo(UserDocsUtil.updateUserData());

    }

    @Test
    public void shouldGetAllUserTokens() throws Exception {

        mockMvc.perform(get("/user/tokens").header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$").isArray())
                .andDo(UserDocsUtil.getUserTokens());

    }

    @Test
    public void shouldDeleteAllUserTokens() throws Exception {

        mockMvc.perform(delete("/user/tokens").header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.deleteCount").value(1))
                .andDo(UserDocsUtil.deleteUserTokens());

    }

    @Test
    public void shouldUpdateUserPassword() throws Exception {

        Gson gson = new Gson();
        NewPasswordDTO newPassword = new NewPasswordDTO("NewPassword");

        String encPasswd = PasswordGenerator.generate("NewPassword");

        mockMvc.perform(put("/user/changepassword").contentType(MediaType.APPLICATION_JSON).header(header, token.getToken()).content(gson.toJson(newPassword)))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(UserDocsUtil.changePasswordOfLoggedUser());


    }
}
