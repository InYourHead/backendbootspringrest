package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.ProductDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.ProductController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 26.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, ProductController.class})
@Transactional
@ActiveProfiles("test")
@PropertySource(value = "classpath:application.properties")
public class ProductControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Value("${token.header}")
    private String header;

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");


    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldReturnAllProductsOfSelectedStore() throws Exception {

        mockMvc.perform(get("/store/{storeId}/products", 1).header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$").isArray())
                .andDo(ProductDocsUtil.getAllProducts());

    }

    @Test
    public void shouldDeleteAllProductsOfSelectedStore() throws Exception {

        mockMvc.perform(delete("/store/{storeId}/products", 1).header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.deleteCount").value(3))
                .andDo(ProductDocsUtil.deleteAllProducts());
    }

    @Test
    public void shouldCreateNewProductInSelectedStore() throws Exception {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        ProduktDTO produkt = new ProduktDTO(null, 2, null, null, "kotlety", new Double(3.99), "pyszne kotleciki wieprzowe!", "");

        String json = gson.toJson(produkt);


        mockMvc.perform(post("/store/{storeId}/products", 1).header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.id_prod_szczeg").isNotEmpty())
                .andDo(ProductDocsUtil.createProduct());
    }

    @Test
    public void shouldGetSelectedProductFromSelectedStore() throws Exception {

        mockMvc.perform(get("/store/{storeId}/products/{productId}", 1, 4).header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id_prod_szczeg").value(4))
                .andDo(ProductDocsUtil.getSelectedProduct());
    }

    @Test
    public void shouldDeleteSelectedProduct() throws Exception {

        mockMvc.perform(delete("/store/{storeId}/products/{productId}", 1, 4).header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.deleteCount").value(1))
                .andDo(ProductDocsUtil.deleteSelectedProduct());

    }

    @Test
    public void shoudUpdateSelectedProduct() throws Exception {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        ProduktDTO produkt = new ProduktDTO(null, 3, null, null, "Nowa nazwa", new Double(4.99), "Nowy opis", "");

        String json = gson.toJson(produkt);


        mockMvc.perform(patch("/store/{storeId}/products/{productId}", 1, 4).header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.nazwa").value("Nowa nazwa"))
                .andDo(ProductDocsUtil.updateSelectedProduct());

    }

}
