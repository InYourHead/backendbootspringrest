package com.inyourhead;

import com.inyourhead.SpringDocsUtils.LoginDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.LoginController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Created by aleksander on 05.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, LoginController.class})
@Transactional
@ActiveProfiles("test")
@PropertySource(value = "classpath:application.properties")
public class LoginControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    private DataRepository repository;

    @Autowired
    private TokenRepository tokenRepository;

    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Value("${token.header}")
    private String header;

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");


    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldLogInExistingUser() throws Exception {

        mockMvc.perform(post("/login").header("Authorization", "Basic YWRtaW46YWRtaW4=").header("User-Agent", "android-2133sad12"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andDo(LoginDocsUtil.login());

    }

    @Test
    public void shouldLogOutExistingUser() throws Exception {


        mockMvc.perform(get("/logout").header(header, token.getToken()))
                .andExpect(status().isOk())
                .andDo(print())
                .andDo(LoginDocsUtil.logout());
        assertNull(tokenRepository.findByToken(token.getToken()));
    }

}
