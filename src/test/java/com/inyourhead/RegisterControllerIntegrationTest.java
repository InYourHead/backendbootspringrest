package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.RegisterDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.web.RegisterController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 05.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, RegisterController.class})
@Transactional
@ActiveProfiles("test")
public class RegisterControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldRegisterNewUser() throws Exception {

        UzytkownikDTO user = new UzytkownikDTO();
        user.setLogin("kowalski");
        user.setHaslo("password");
        user.setAdres_kod("2");
        user.setAdres_miasto("Bydgoszczy");
        user.setAdres_nr_domu("27");
        user.setAdres_ulica("Kwiatowa");
        user.setAdres_woj("Kujawsko-Pomorskie");
        user.setEmail("a.h.burzec@gmail.pl");
        user.setImie("Aleksander");
        user.setNazwisko("Burzec");
        user.setTelefon("700333322");
        user.setTyp(1);

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

        String content = gson.toJson(user);


        mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON_VALUE).content(content))
                .andDo(print())
                .andExpect(jsonPath("$.login").isNotEmpty())
                .andDo(RegisterDocsUtil.registerNewUser());


    }

    @Test
    public void shouldSendResetPasswordEmail() throws Exception {

        mockMvc.perform(get("/changepassword?login=admin"))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(RegisterDocsUtil.sendResetPasswordEmail());

    }
}
