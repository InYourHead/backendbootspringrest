package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.StoreDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.StoreController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 06.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, StoreController.class})
@Transactional
@ActiveProfiles("test")
@PropertySource(value = {"classpath:application.properties"})
public class StoreControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Value("${token.header}")
    private String header;

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");

    private SklepDTO newStore;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldGetAllStores() throws Exception {

        mockMvc.perform(get("/store").header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(StoreDocsUtil.getAllStores());


    }

    @Test
    public void shouldCreateNewStore() throws Exception {

        newStore = new SklepDTO("87-100", "Torun", "5", "Wąska", "Kujawsko-Pomorskie", null, "Auchan", "", "https://www.facebook.com/auchan/?fref=ts", "http://www.auchan.pl/", new Double(18.584295), new Double(53.020819), 1, null);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jstore = gson.toJson(newStore);


        mockMvc.perform(post("/store").header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON_VALUE).content(jstore))
                .andDo(print())
                .andExpect(status().isCreated())
                .andDo(StoreDocsUtil.createNewStore());
    }

    @Test
    public void shouldDeleteAllStoriesOfSelectedUser() throws Exception {

        mockMvc.perform(delete("/store").header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleteCount").value(3))
                .andDo(StoreDocsUtil.deleteAllStores());

    }

    @Test
    public void shouldGetSelectedStore() throws Exception {

        mockMvc.perform(get("/store/{storeId}", 1).header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nazwa").value("Biedronka"))
                .andDo(StoreDocsUtil.getSelectedStore());

    }

    @Test
    public void shouldUpdateSelectedStore() throws Exception {

        SklepDTO sklep = new SklepDTO();

        sklep.setNazwa("Polo");

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

        String content = gson.toJson(sklep);


        mockMvc.perform(patch("/store/{storeId}", 1).header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nazwa").value("Polo"))
                .andDo(StoreDocsUtil.updateStore());

    }

    @Test
    public void shouldDeleteSelectedStore() throws Exception {

        mockMvc.perform(delete("/store/{storeId}", 1).header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleteCount").value(1))
                .andDo(StoreDocsUtil.deleteSelectedStore());

    }
}
