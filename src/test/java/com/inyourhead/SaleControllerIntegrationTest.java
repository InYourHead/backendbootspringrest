package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.SaleDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.PromocjaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.SaleController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 28.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, SaleController.class})
@Transactional
@ActiveProfiles("test")
@PropertySource(value = "classpath:application.properties")
public class SaleControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Value("${token.header}")
    private String header;

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldGetSaleInfoAboutProduct() throws Exception {

        mockMvc.perform(get("/store/{storeId}/products/{productId}/sale", 1, 4).header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cena_promocyjna").value(10.99))
                .andDo(SaleDocsUtil.getSale());
    }

    @Test
    public void shouldDeleteSale() throws Exception {

        mockMvc.perform(delete("/store/{storeId}/products/{productId}/sale", 1, 4).header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleteCount").value(1))
                .andDo(SaleDocsUtil.deleteSale());
    }

    @Test
    public void shouldUpdateSale() throws Exception {

        PromocjaDTO promocja = new PromocjaDTO();

        promocja.setCena_promocyjna(new Double(299.99));

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String json = gson.toJson(promocja);


        mockMvc.perform(patch("/store/{storeId}/products/{productId}/sale", 1, 4).header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cena_promocyjna").value(299.99))
                .andDo(SaleDocsUtil.updateSale());

    }

    @Test
    public void shouldCreateSale() throws Exception {

        PromocjaDTO promocja = new PromocjaDTO();

        promocja.setCena_promocyjna(new Double(299.99));

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String json = gson.toJson(promocja);


        mockMvc.perform(post("/store/{storeId}/products/{productId}/sale", 1, 5).header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.cena_promocyjna").value(299.99))
                .andDo(SaleDocsUtil.createSale());

    }

}
