package com.inyourhead;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.SpringDocsUtils.SearchDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.web.HistoryController;
import com.inyourhead.backend.web.SearchController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 29.12.16.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, HistoryController.class, SearchController.class})
@Transactional("transactionManager")
@ActiveProfiles("test")
@PropertySource(value = "classpath:application.properties")

public class SearchControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Value("${token.header}")
    private String header;

    private ZapytanieDTO zapytanieDTO;

    private Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    private Token token = new Token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.1FKlZBwF5wg3Zxb0zj6IE6bxx5hafVQDMur9yLNFkOA");

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldGetProductResultsAsLoggedInUser() throws Exception {

        zapytanieDTO = new ZapytanieDTO();

        zapytanieDTO.setFraza("kebab");
        zapytanieDTO.setId_kat(null);
        zapytanieDTO.setLat(new Double(53.106108));
        zapytanieDTO.setLng(new Double(18.028888));

        String json = gson.toJson(zapytanieDTO);

        mockMvc.perform(post("/search?size=10&desc=1").contentType(MediaType.APPLICATION_JSON).content(json).header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(SearchDocsUtil.getProductResultAsLoggedInUser());
    }


    @Test
    public void shouldGetProductResultsAsLoggedInUserUsingGET() throws Exception {

        mockMvc.perform(get("/search?fraza=kebab&lat=53&lng=18&size=10&desc=1").header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(SearchDocsUtil.getProductResultAsLoggedInUserUsingGET());
    }

    @Test
    public void shouldGetStoreResultAsLoggedInUserUsingPatchParameters() throws Exception {

        mockMvc.perform(get("/search?fraza=Biedronka&id_kat=1&lat=53.106108&lng=18.028888").header(header, token.getToken()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(SearchDocsUtil.getStoreResultAsLoggedInUserUsingGET());
    }

    @Test
    public void shouldGetStoreResultAsLoggedUserAndUpdateHistory() throws Exception {

        zapytanieDTO = new ZapytanieDTO();

        zapytanieDTO.setFraza("Biedronka");
        zapytanieDTO.setId_kat(1);
        zapytanieDTO.setLat(new Double(53.106108));
        zapytanieDTO.setLng(new Double(18.028888));


        String json = gson.toJson(zapytanieDTO);

        mockMvc.perform(post("/search").header(header, token.getToken()).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(SearchDocsUtil.getStoreResultAsLoggedInUser());
    }

}
