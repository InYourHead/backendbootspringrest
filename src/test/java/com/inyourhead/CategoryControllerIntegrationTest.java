package com.inyourhead;

import com.google.gson.Gson;
import com.inyourhead.SpringDocsUtils.CategoryDocsUtil;
import com.inyourhead.backend.configuration.database.JpaTestConfig;
import com.inyourhead.backend.configuration.database.JpaTokenConfig;
import com.inyourhead.backend.configuration.security.SecurityConfig;
import com.inyourhead.backend.configuration.security.filters.JwtAuthenticationTokenFilter;
import com.inyourhead.backend.configuration.security.filters.TokenFilterRegistrationConfig;
import com.inyourhead.backend.data.dto.DefaultProductDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.scheduledTasks.EmbeddedDatabaseImagesInitializer;
import com.inyourhead.backend.web.CategoryController;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 30.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {JpaTokenConfig.class, JpaTestConfig.class, SecurityConfig.class, TokenFilterRegistrationConfig.class, JwtAuthenticationTokenFilter.class, EmailService.class, EmbeddedDatabaseImagesInitializer.class, CategoryController.class})
@ActiveProfiles("test")
@Transactional
public class CategoryControllerIntegrationTest {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");
    @Autowired
    DataRepository repository;
    @Resource
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(this.restDocumentation).uris()
                        .withScheme("https")
                        .withHost("localhost")
                        .withPort(8443))
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldReturnAllCategories() throws Exception {

        mockMvc.perform(get("/categories"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty())
                .andDo(CategoryDocsUtil.getCategories());


    }

    @Test
    public void shouldReturnAllDistricts() throws Exception {

        mockMvc.perform(get("/provinces"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(CategoryDocsUtil.getProvinces());


    }

    @Test
    public void shouldReturnAllDefaultProducts() throws Exception {

        mockMvc.perform(get("/defaultproducts"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(CategoryDocsUtil.getDefaultProducts());

    }

    @Test
    public void shouldCreateNewDefaultProduct() throws Exception {

        DefaultProductDTO json = new DefaultProductDTO(null, "Pieczywo", "Wypieki piekarniane.", "", 3);

        Gson gson = new Gson();

        mockMvc.perform(post("/defaultproducts").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(json)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andDo(CategoryDocsUtil.createDefaultProduct());

    }

    @Test
    public void getProductsOfSelectedStore() throws Exception {

        mockMvc.perform(get("/storeproducts?storeId=1&size=3").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(CategoryDocsUtil.getProductsOfSelectedStore());


    }
}
