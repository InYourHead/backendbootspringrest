package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityExistsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.facade.impl.RegisterFacadeImpl;
import com.inyourhead.backend.facade.interfaces.RegisterFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by aleksander on 30.11.16.
 */
@RestController
public class RegisterController {


    private RegisterFacade registerFacade;
    private EmailService emailService;


    @Autowired
    public RegisterController(DataRepository repository, EmailService emailService, TokenRepository tokenRepository, Environment env) {

        this.registerFacade = new RegisterFacadeImpl(repository, emailService, tokenRepository, env);
        this.emailService = emailService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<UzytkownikDTO> createNewUser(@RequestBody UzytkownikDTO user) throws EntityExistsException, EmptyFieldsException {

        UzytkownikDTO createdUser = null;

        createdUser = registerFacade.createNewUser(user);

        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public void getResetPasswordEmail(@RequestParam(value = "login", required = true) String login) throws EntityNotFoundException {

        registerFacade.sendResetUserPasswordEmailUsingLoginOrEmail(login);

    }
}
