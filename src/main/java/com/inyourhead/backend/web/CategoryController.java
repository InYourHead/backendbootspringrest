package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DefaultProductDTO;
import com.inyourhead.backend.data.dto.DistrictDTO;
import com.inyourhead.backend.data.dto.KategoriaDTO;
import com.inyourhead.backend.data.dto.WynikProduktObiektDTOImpl;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.facade.impl.CategoryFacadeImpl;
import com.inyourhead.backend.facade.interfaces.CategoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by aleksander on 30.12.16.
 */
@RestController
public class CategoryController {

    private CategoryFacade categoryFacade;

    @Autowired
    public CategoryController(DataRepository repository, Environment env) {

        this.categoryFacade = new CategoryFacadeImpl(repository, env);

    }

    @RequestMapping(path = "/categories", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<KategoriaDTO>> getAllCategories() {

        List<KategoriaDTO> response = null;

        response = categoryFacade.getAllCategories();

        response.remove(0);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(path="/provinces", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DistrictDTO>> getAllProvinces() {

        List<DistrictDTO> response = null;

        response = categoryFacade.getAllProvinces();

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @RequestMapping(path = "/defaultproducts", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DefaultProductDTO>> getDefaultProductsList() {

        List<DefaultProductDTO> response = null;

        response = categoryFacade.getAllDefaultProducts();

        return new ResponseEntity(response, HttpStatus.OK);

    }

    @RequestMapping(path = "/defaultproducts", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<DefaultProductDTO> createDefaultProduct(@RequestBody DefaultProductDTO newDefaultProduct) throws EntityNotFoundException {
        DefaultProductDTO response = null;

        response = categoryFacade.createDefaultProduct(newDefaultProduct);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/storeproducts", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<WynikProduktObiektDTOImpl>> getProductsOfSelectedStore(@RequestParam("storeId") Integer storeId, @RequestParam(value = "size", required = false, defaultValue = "8") Integer size) {

        List<WynikProduktObiektDTOImpl> response = null;

        response = categoryFacade.getProductsOfSelectedStore(storeId, size);

        return new ResponseEntity<>(response, HttpStatus.OK);


    }
}
