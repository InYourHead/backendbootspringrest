package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.impl.ProductFacadeImpl;
import com.inyourhead.backend.facade.interfaces.ProductFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 26.12.16.
 */
@RestController
@RequestMapping("/store/{storeId}/products")
public class ProductController {

    private ProductFacade productFacade;


    @Autowired
    public ProductController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.productFacade = new ProductFacadeImpl(repository, tokenRepository, env);

    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<ProduktDTO>> getAllProductsOfSelectedStore(@PathVariable("storeId") Integer storeId, ServletRequest request) throws EntityNotFoundException, UserNotPermittedException {

        List<ProduktDTO> response;

        response = productFacade.getAllProducts(request, storeId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteAllProductsOfSelectedStore(@PathVariable("storeId") Integer storeId, ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {

        DeleteDetails response;

        response = productFacade.deleteAllProductsOfSelectedStore(request, storeId);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProduktDTO> createNewProductToSelectedStore(@PathVariable("storeId") Integer storeId, @RequestBody ProduktDTO product, ServletRequest request) throws UserNotPermittedException, EntityNotCreatedException, EntityNotFoundException {
        ProduktDTO response;

        response = productFacade.addNewProductToSelectedStore(request, storeId, product);

        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<ProduktDTO> getSelectedProductOfSelectedStore(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, ServletRequest request) throws UserNotPermittedException {
        ProduktDTO response;

        response = productFacade.getSelectedProduct(request, storeId, productId);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteSelectedProductOfSelectedStore(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        response = productFacade.deleteSelectedProduct(request, storeId, productId);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProduktDTO> updateSelectedProductFromSelectedStore(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, @RequestBody ProduktDTO product, ServletRequest request) throws UserNotPermittedException, EntityNotFoundException, EntityNotUpdatedException {
        ProduktDTO response;

        response = productFacade.updateSelectedProduct(request, storeId, productId, product);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
