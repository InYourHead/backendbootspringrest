package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.NotValidQueryException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.impl.SearchFacadeImpl;
import com.inyourhead.backend.facade.interfaces.SearchFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
@RestController
@RequestMapping("/search")
public class SearchController {

    private SearchFacade searchFacade;

    @Autowired
    public SearchController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.searchFacade = new SearchFacadeImpl(repository, tokenRepository, env);

    }
	
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<List<WynikDTO>> getSearchResults(@RequestParam(value = "size", required = false) Integer size, @RequestBody ZapytanieDTO szukaj, @RequestParam(value = "desc", required = false, defaultValue = "false") boolean desc, ServletRequest request) throws NotValidQueryException, EntityNotFoundException, UserNotPermittedException {

        List<WynikDTO> searchResult;

        searchResult = searchFacade.search(szukaj, size, desc, request);

        return new ResponseEntity<>(searchResult, HttpStatus.OK);
    }
	
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<WynikDTO>> getSearchResultsUsingGET(ZapytanieDTO szukaj, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "desc", required = false, defaultValue = "false") boolean desc, ServletRequest request) throws NotValidQueryException, EntityNotFoundException, UserNotPermittedException {

        List<WynikDTO> searchResult;

        searchResult = searchFacade.search(szukaj, size, desc, request);

        return new ResponseEntity<>(searchResult, HttpStatus.OK);
    }
}
