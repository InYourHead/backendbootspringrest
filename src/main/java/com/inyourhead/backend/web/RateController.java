package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.OcenaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.EntityNotUpdatedException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.impl.RateFacadeImpl;
import com.inyourhead.backend.facade.interfaces.RateFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 06.04.17.
 */
@RestController
@RequestMapping("/rateproduct")
public class RateController {

    private RateFacade rateFacade;


    @Autowired
    public RateController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.rateFacade = new RateFacadeImpl(repository, tokenRepository, env);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<OcenaDTO> rateSelectedProduct(@PathVariable("productId") Integer productId, @RequestBody OcenaDTO rate, ServletRequest request) throws EmptyFieldsException, UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException {
        OcenaDTO response;

        response = rateFacade.rateProduct(request, productId, rate);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<OcenaDTO> getRateOfSelectedProduct(@PathVariable("productId") Integer productId, ServletRequest request) throws UserNotPermittedException {
        OcenaDTO response;

        response = rateFacade.getUserRateOfSelectedProduct(request, productId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
