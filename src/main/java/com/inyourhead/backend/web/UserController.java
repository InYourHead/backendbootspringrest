package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.NewPasswordDTO;
import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.impl.UserFacadeImpl;
import com.inyourhead.backend.facade.interfaces.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 08.12.16.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private UserFacade userFacade;

    @Autowired
    public UserController(DataRepository repository, TokenRepository tokenRepository, Environment env) {

        userFacade = new UserFacadeImpl(repository, tokenRepository, env);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<UzytkownikDTO> getUserInfo(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {

        UzytkownikDTO response = null;
        response = userFacade.getUserInfo(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteAccount(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response = null;

        response = userFacade.deleteUserAccount(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
    public ResponseEntity<UzytkownikDTO> updateUserData(@RequestBody UzytkownikDTO user, ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {
        UzytkownikDTO response = null;

        response = userFacade.updateUserData(request, user);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/tokens", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<TokenDTO>> getTokens(ServletRequest request) throws UserNotPermittedException {
        List<TokenDTO> response = null;

        response = userFacade.getTokens(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/tokens", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteTokens(ServletRequest request) throws UserNotPermittedException {
        DeleteDetails response = null;

        response = userFacade.deleteTokens(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/changepassword", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public void changePassword(ServletRequest request, @RequestBody NewPasswordDTO newPassword) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, NotValidQueryException {

        userFacade.changePassword(request, newPassword);
    }
}
