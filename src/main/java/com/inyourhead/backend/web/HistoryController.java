package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.HistoriaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EntityNotDeletedException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.impl.HistoryFacadeImpl;
import com.inyourhead.backend.facade.interfaces.HistoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 25.12.16.
 */
@RestController
@RequestMapping("/user/history")
public class HistoryController {

    private HistoryFacade historyFacade;

    @Autowired
    public HistoryController(DataRepository repository, TokenRepository tokenRepository, Environment env) {

        this.historyFacade = new HistoryFacadeImpl(repository, tokenRepository, env);

    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<HistoriaDTO>> getHistory(ServletRequest request) throws UserNotPermittedException {
        List<HistoriaDTO> response;

        response = historyFacade.getHistory(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteHistory(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        response = historyFacade.deleteHistory(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
