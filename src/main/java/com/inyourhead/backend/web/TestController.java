package com.inyourhead.backend.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by aleksander on 27.11.16.
 */
@RestController
@RequestMapping("/")
public class TestController {

    @RequestMapping(method = RequestMethod.GET)
    public String getHelloMessage(){
        return "It's working!";
    }
}
