package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.impl.StoreFacadeImpl;
import com.inyourhead.backend.facade.interfaces.StoreFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
@RestController
@RequestMapping("/store")
public class StoreController {

    private StoreFacade storeFacade;

    @Autowired
    public StoreController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.storeFacade = new StoreFacadeImpl(repository, tokenRepository, env);

    }


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<SklepDTO>> getAllStores(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {

        List<SklepDTO> response;

        response = storeFacade.getAllStories(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public ResponseEntity<SklepDTO> createStore(@RequestBody SklepDTO store, ServletRequest request) throws UserNotPermittedException, EntityNotCreatedException, NotValidQueryException, EntityNotFoundException {

        SklepDTO response;

        response = storeFacade.createNewStore(request, store);

        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteAllStories(ServletRequest request) throws EntityNotDeletedException, UserNotPermittedException, NotValidQueryException {

        DeleteDetails response;

        response = storeFacade.deleteAllStoriesOfSelectedUser(request);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = "/{storeId}", method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<SklepDTO> getSelectedStore(@PathVariable("storeId") Integer storeId, ServletRequest request) throws EntityNotFoundException, UserNotPermittedException {

        SklepDTO response;

        response = storeFacade.findStoreByUserIdAndStoreId(request, storeId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{storeId}", method = RequestMethod.PATCH,consumes = "application/json", produces = "application/json")
    public ResponseEntity<SklepDTO> updateSelectedStore(@PathVariable("storeId") Integer storeId, @RequestBody SklepDTO store, ServletRequest request) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException {

        SklepDTO response;

        response = storeFacade.updateSelectedStore(request, store, storeId);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(value = "/{storeId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteSelectedStore(@PathVariable("storeId") Integer storeId, ServletRequest request) throws EntityNotDeletedException, UserNotPermittedException {

        DeleteDetails response;

        response = storeFacade.deleteSelectedStoryOfSelectedUser(request, storeId);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }
}
