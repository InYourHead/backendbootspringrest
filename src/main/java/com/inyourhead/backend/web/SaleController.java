package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.PromocjaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.impl.SaleFacadeImpl;
import com.inyourhead.backend.facade.interfaces.SaleFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 28.12.16.
 */
@RestController
@RequestMapping("/store/{storeId}/products/{productId}/sale")
public class SaleController {

    private SaleFacade saleFacade;

    @Autowired
    public SaleController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.saleFacade = new SaleFacadeImpl(repository, tokenRepository, env);

    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<PromocjaDTO> getSaleForSelectedProduct(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {
        PromocjaDTO response;

        response = saleFacade.getSale(request, storeId, productId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<DeleteDetails> deleteSaleForSelectedProduct(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        response = saleFacade.deleteSale(request, storeId, productId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, produces = "application/json")
    public ResponseEntity<PromocjaDTO> updateSaleForSelectedProduct(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, @RequestBody PromocjaDTO sale, ServletRequest request) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException {
        PromocjaDTO response;

        response = saleFacade.updateSale(request, storeId, productId, sale);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<PromocjaDTO> createSaleForSelectedProduct(@PathVariable("storeId") Integer storeId, @PathVariable("productId") Integer productId, @RequestBody PromocjaDTO sale, ServletRequest request) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, EntityNotCreatedException, EntityExistsException {
        PromocjaDTO response;

        response = saleFacade.createSale(request, storeId, productId, sale);

        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }
}
