package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.ErrorResponse;
import com.inyourhead.backend.exceptions.*;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by aleksander on 26.04.17.
 */
@ControllerAdvice
public class ErrorHandlingAdvice {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> integrityViolationExceptionHandler(Exception ex) {
        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(500);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UserNotPermittedException.class)
    public ResponseEntity<ErrorResponse> notPermittedExceptionHandler(Exception ex) {

        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(403);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> notFoundExceptionHandler(Exception ex) {

        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(404);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {EntityNotDeletedException.class, EntityNotUpdatedException.class})
    public ResponseEntity<ErrorResponse> notDeletedOrUpdatedExceptionHandler(Exception ex) {

        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(500);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EntityNotCreatedException.class)
    public ResponseEntity<ErrorResponse> notCreatedException(Exception ex) {

        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(400);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler({EmptyFieldsException.class, EntityExistsException.class})
    public ResponseEntity<ErrorResponse> emptyFieldsExceptionHandler(Exception ex) {
        ErrorResponse response = new ErrorResponse();

        response.setHttpStatusCode(422);

        response.setDescription(ex.getMessage());

        return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
    }


}
