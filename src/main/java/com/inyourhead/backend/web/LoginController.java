package com.inyourhead.backend.web;

import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.impl.LoginFacadeImpl;
import com.inyourhead.backend.facade.interfaces.LoginFacade;
import com.inyourhead.backend.utils.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import java.security.Principal;

/**
 * Created by aleksander on 29.11.16.
 */
@RestController
public class LoginController {

    private LoginFacade loginFacade;


    @Autowired
    public LoginController(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        loginFacade = new LoginFacadeImpl(repository, tokenRepository, env);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Token> getAuthenticationToken(Principal principal, ServletRequest request) {
        Token response;

        response = loginFacade.getTokenForLoggedUser(principal, request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<TokenDTO> logoutUser(ServletRequest request) throws UserNotPermittedException {
        TokenDTO response;

        response = loginFacade.logoutUser(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
