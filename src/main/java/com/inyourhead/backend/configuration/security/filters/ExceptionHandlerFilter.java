package com.inyourhead.backend.configuration.security.filters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inyourhead.backend.data.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by aleksander on 02.01.17.
 */
public class ExceptionHandlerFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (ServletException e) {
            ErrorResponse errorResponse = new ErrorResponse();

            errorResponse.setDescription(e.getMessage());
            errorResponse.setHttpStatusCode(HttpStatus.FORBIDDEN.value());
            Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.getWriter().write(gson.toJson(errorResponse));
        }
    }
}
