package com.inyourhead.backend.configuration.security.filters;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by aleksander on 30.11.16.
 */
@Configuration
public class TokenFilterRegistrationConfig {

    private String patterns[] = {"/store", "/store/**", "/logout", "/user", "/user/**", "/rateproduct/**"};

    @Bean
    public FilterRegistrationBean jwtRegistrationBean() throws Exception {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(authenticationTokenFilterBean());
        registrationBean.setOrder(2);
        registrationBean.addUrlPatterns(patterns);
        return registrationBean;
    }

    @Bean
    FilterRegistrationBean exceptionHandlerRegistrationBean() throws Exception {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(exceptionTokenFilterBean());
        registrationBean.setOrder(1);
        registrationBean.addUrlPatterns(patterns);
        return registrationBean;

    }

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        return new JwtAuthenticationTokenFilter();
    }

    @Bean
    public ExceptionHandlerFilter exceptionTokenFilterBean() {
        return new ExceptionHandlerFilter();
    }
}
