package com.inyourhead.backend.configuration.security.filters;

import com.inyourhead.backend.data.entities.Token;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by aleksander on 29.11.16.
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    final private String HEADER = "token.header";

    @Autowired
    private Environment env;

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain) throws ServletException, IOException {

        final HttpServletRequest request = req;

        final String authHeader = request.getHeader(env.getProperty(HEADER));
        System.out.println(authHeader);
        if (authHeader == null) {
            throw new ServletException("Brakujacy header z tokenem!");
        }

        final String token = authHeader;// The part after "Bearer "

        System.out.println(token);

        try {
            Token claims = tokenRepository.findByToken(token);
            final String login = Jwts.parser().setSigningKey(claims.getSingingKey())
                    .parseClaimsJws(token).getBody().getSubject();

        } catch (final SignatureException | MalformedJwtException e) {
            throw new ServletException(e.getMessage());
        } catch (NullPointerException ex) {
            throw new ServletException("Blad przy uwierzytelnieniu tokena! Spróbuj zalogować sie jeszcze raz!");
        }

        filterChain.doFilter(req, res);
    }

    public void destroy() {

    }
}
