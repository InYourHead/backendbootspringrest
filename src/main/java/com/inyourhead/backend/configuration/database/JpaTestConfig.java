package com.inyourhead.backend.configuration.database;

import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;

/**
 * Created by aleksander on 04.12.16.
 */
@Configuration
@PropertySource("classpath:testApplication.properties")
@EnableJpaRepositories(basePackages = {"com.inyourhead.backend.data"}, transactionManagerRef = "transactionManager", entityManagerFactoryRef = "entityManagerFactory")
@Profile({"test", "tomcat"})
public class JpaTestConfig {

    private final String packages = "com.inyourhead.backend.data";

    @Bean
    public DataSource dataSource(){

        EmbeddedDatabaseBuilder builder= new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db= builder
                .setType(EmbeddedDatabaseType.H2)
                .setName("matumk_db")
                .addScript("classpath:database/test/create.sql")
                .addScript("classpath:database/test/insert-data.sql")
                .addScript("classpath:database/production/create_token_storage.sql")
                .addScript("classpath:database/test/insert-tokens.sql")
                .build();

        return db;
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        vendorAdapter.setShowSql(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan(packages);
        factory.setDataSource(dataSource());
        return factory;
    }

    @Bean
    JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

}
