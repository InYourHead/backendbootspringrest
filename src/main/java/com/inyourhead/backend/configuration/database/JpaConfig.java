/**
 *
 */
package com.inyourhead.backend.configuration.database;

import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.DataRepositoryCustom;
import com.inyourhead.backend.data.repositories.DataRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author aleksander
 *
 */
@Configuration
@PropertySource("classpath:productionApplication.properties")
@EnableJpaRepositories(basePackages = {"com.inyourhead.backend.data.entities", "com.inyourhead.backend.data.mappingEntities"}, basePackageClasses = {DataRepository.class, DataRepositoryCustom.class, DataRepositoryImpl.class}, entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager")
@EnableTransactionManagement
@Profile("production")
@SuppressWarnings("javadoc")
public class JpaConfig
{
    private static final String PROPERTY_NAME_DATABASE_DRIVER = "spring.datasource.driver-class-name";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD = "spring.datasource.password";
    private static final String PROPERTY_NAME_DATABASE_URL = "spring.datasource.url";
	private static final String PROPERTY_NAME_DATABASE_USERNAME = "spring.datasource.username";
    private final String packages = "com.inyourhead.backend.data";

    @Autowired
    private Environment env;

	@Bean(name="dataSource")
	public DataSource dataSource() {

		DataSourceBuilder builder = DataSourceBuilder.create();
		builder.driverClassName(env.getProperty(PROPERTY_NAME_DATABASE_DRIVER));
		builder.url(env.getProperty(PROPERTY_NAME_DATABASE_URL));
		builder.username(env.getProperty(PROPERTY_NAME_DATABASE_USERNAME));
		builder.password(env.getProperty(PROPERTY_NAME_DATABASE_PASSWORD));

		return builder.build();
	}

    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("dataSource") DataSource mySqlDataSource) {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(packages);
        factory.setDataSource(mySqlDataSource);
        factory.setJpaProperties(additionalJpaProperties());


		return factory;
	}

    public Properties additionalJpaProperties() {

        Properties properties = new Properties();

        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");

        return properties;

    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager mySqlTransactionManager() {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(
                entityManagerFactory(dataSource()).getObject());
        return txManager;
    }
}
