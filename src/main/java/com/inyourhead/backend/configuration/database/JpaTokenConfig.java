package com.inyourhead.backend.configuration.database;

import com.inyourhead.backend.data.entities.Token;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by aleksander on 04.12.16.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = {TokenRepository.class, Token.class}, entityManagerFactoryRef = "h2entityManagerFactory", transactionManagerRef = "h2TokenTransactionManager")
public class JpaTokenConfig {

    private final String packages = "com.inyourhead.backend.data";

    @Bean
    @Primary
    public DataSource h2TokenDataSource() {

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.H2)
                .setName("matumk_db")
                .addScript("classpath:database/production/create_token_storage.sql")
                .build();

        return db;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean h2entityManagerFactory() {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan(packages);
        factory.setDataSource(h2TokenDataSource());
        return factory;
    }

    @Bean(name = "h2TokenTransactionManager")
    JpaTransactionManager transactionManager(@Qualifier("h2TokenDataSource") DataSource dataSource) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(h2entityManagerFactory().getObject());
        return transactionManager;
    }

}
