package com.inyourhead.backend.utils;

import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import java.time.Instant;
import java.util.Random;

/**
 * Created by aleksander on 13.04.17.
 */
@Service
public class TokenGenerator {

    private DataRepository dataRepository;
    private TokenRepository tokenRepository;
    private ClaimsUtils claimsUtils;
    private Environment env;

    @Autowired
    public TokenGenerator(DataRepository dataRepository, TokenRepository tokenRepository, Environment env) {
        this.dataRepository = dataRepository;
        this.tokenRepository = tokenRepository;
        this.env = env;
        this.claimsUtils = new ClaimsUtils(env, tokenRepository, dataRepository);
    }


    public TokenGenerator() {
        super();
    }

    public Token createToken(String userLogin, ServletRequest request) {

        com.inyourhead.backend.data.entities.Token tokenEntity = new com.inyourhead.backend.data.entities.Token();
        String deviceId;

        //pobierzmy user id
        Integer userId = dataRepository.findUserIdByLogin(userLogin);

        //pobieramy czas od poczatku unixa
        long unixTimeStamp = Instant.now().getEpochSecond();

        //ustawmy deviceId
        if (request != null) {
            deviceId = claimsUtils.getUserDevice(request);
        } else {
            deviceId = "RESET_PASSWORD";
        }

        //ustawmy singing key
        String singingKey = String.format("%d%s%d", unixTimeStamp, deviceId, (new Random()).nextLong());

        Token authToken = new Token(Jwts.builder().signWith(SignatureAlgorithm.HS256, singingKey).setSubject(userLogin)
                .compact());

        tokenEntity.setDeviceId(deviceId);
        tokenEntity.setSingingKey(singingKey);
        tokenEntity.setUserId(userId);
        tokenEntity.setToken(authToken.getToken());

        tokenRepository.save(tokenEntity);

        return authToken;
    }
}
