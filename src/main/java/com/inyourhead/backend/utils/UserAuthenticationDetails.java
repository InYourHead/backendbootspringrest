package com.inyourhead.backend.utils;

/**
 * Created by aleksander on 29.11.16.
 */
public class UserAuthenticationDetails {

    private String login;
    private Integer userId;
    private Integer accountType;
    private String token;


    public UserAuthenticationDetails() {

    }

    public UserAuthenticationDetails(String login, Integer userId, Integer accountType, String token) {
        this.login = login;
        this.userId = userId;
        this.accountType = accountType;
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
