package com.inyourhead.backend.utils;

import java.util.Scanner;

public class PasswordHasher
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		String password;
		password=scanner.nextLine();
		System.out.println(PasswordGenerator.generate(password));
	}
}
