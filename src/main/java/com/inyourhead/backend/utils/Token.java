package com.inyourhead.backend.utils;

/**
 * Created by aleksander on 29.11.16.
 */
public class Token {

    private String token;

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
