package com.inyourhead.backend.utils.enums;

/**
 * Created by aleksander on 01.12.16.
 */
public class UserRole {

    final public static Integer ADMIN = 1;

    final public static Integer USER = 0;
}
