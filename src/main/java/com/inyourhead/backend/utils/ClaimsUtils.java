package com.inyourhead.backend.utils;

import com.inyourhead.backend.data.entities.Token;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by aleksander on 04.12.16.
 */
public class ClaimsUtils {

    private String TOKEN_HEADER;
    private String AGENT_DEVICE_HEADER;
    private DataRepository dataRepository;
    private TokenRepository tokenRepository;
    private Environment env;


    @Autowired
    public ClaimsUtils(Environment env, TokenRepository tokenRepository, DataRepository repository)
    {
        this.env=env;
        this.tokenRepository = tokenRepository;
        this.dataRepository=repository;
        TOKEN_HEADER = env.getProperty("token.header");
        AGENT_DEVICE_HEADER = env.getProperty("useragent.header");
    }



    public UserAuthenticationDetails getUserDetails(ServletRequest request) throws UserNotPermittedException {

        String userLogin;
        Integer userId;
        Integer userRole;
        HttpServletRequest req= (HttpServletRequest) request;

        String authHeader = req.getHeader(TOKEN_HEADER);
        try {
            if (authHeader == null || authHeader.equals("")) throw new UserNotPermittedException("Brak tokena!");
            Token token = tokenRepository.findByToken(authHeader);

            if (token == null) throw new UserNotPermittedException("Błąd logowania przy użyciu tokena!");

            userLogin = Jwts.parser().setSigningKey(token.getSingingKey())
                    .parseClaimsJws(authHeader).getBody().getSubject();
        } catch (IllegalArgumentException ex) {
            userLogin = null;
        }

        if(userLogin==null) throw new UserNotPermittedException("login jest pusty!");

        userId = dataRepository.findUserIdByLogin(userLogin);

        if (userId == null) throw new UserNotPermittedException("Podany uzytkownik nie istnieje!");

        userRole = dataRepository.findUserRoleByUserLogin(userLogin);

        return new UserAuthenticationDetails(userLogin, userId, userRole, authHeader);
    }

    public String getUserDevice(ServletRequest request) {

        HttpServletRequest req = (HttpServletRequest) request;

        String devHeader = req.getHeader(AGENT_DEVICE_HEADER);

        if (devHeader == null || devHeader.equals(""))
            return "Nieznane urządzenie";

        return devHeader;

    }
}
