package com.inyourhead.backend.utils;

import java.util.Base64;

/**
 * Created by aleksander on 15.04.17.
 */
public class ImageBase64Converter {

    public static String encodeToString(byte[] source) {

        String response = null;
        if (source != null)
            response = Base64.getEncoder().encodeToString(source);

        return response;
    }

    public static byte[] decodeToByteArray(String source) {

        byte[] response = null;
        if (source != null)
            response = Base64.getDecoder().decode(source);

        return response;
    }

}
