package com.inyourhead.backend.data.dto;

import com.inyourhead.backend.data.entities.Uzytkownik;

/**
 * Created by aleksander on 30.11.16.
 */
public class UzytkownikDTO {

    private String haslo = null;
    private String adres_miasto = null;
    private String imie = null;
    private String email = null;
    private String adres_nr_domu = null;
    private String adres_ulica = null;
    private String login = null;
    private String telefon = null;
    private Integer typ = null;
    private String nazwisko = null;
    private String adres_woj = null;
    private String adres_kod = null;

    public UzytkownikDTO() {

    }

    public UzytkownikDTO(Uzytkownik uzytkownik) {
        this.login = (uzytkownik.getLogin());
        this.haslo = (uzytkownik.getHaslo());
        this.adres_kod = (uzytkownik.getAdresKod());
        this.adres_miasto = (uzytkownik.getAdresMiasto());
        this.adres_nr_domu = (uzytkownik.getAdresNrDomu());
        this.adres_ulica = (uzytkownik.getAdresUlica());
        this.adres_woj = (uzytkownik.getAdresWoj());
        this.email = (uzytkownik.getEmail());
        this.imie = (uzytkownik.getImie());
        this.nazwisko = (uzytkownik.getNazwisko());
        this.telefon = (uzytkownik.getTelefon());
        this.typ = (uzytkownik.getTyp());
    }

    public Uzytkownik toUzytkownik() {
        return new Uzytkownik(null, this.typ, this.imie,
                this.nazwisko, this.email, this.telefon,
                this.adres_ulica, this.adres_nr_domu, this.adres_kod,
                this.adres_miasto, this.adres_woj, this.haslo, this.login);

    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getAdres_miasto() {
        return adres_miasto;
    }

    public void setAdres_miasto(String adres_miasto) {
        this.adres_miasto = adres_miasto;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdres_nr_domu() {
        return adres_nr_domu;
    }

    public void setAdres_nr_domu(String adres_nr_domu) {
        this.adres_nr_domu = adres_nr_domu;
    }

    public String getAdres_ulica() {
        return adres_ulica;
    }

    public void setAdres_ulica(String adres_ulica) {
        this.adres_ulica = adres_ulica;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Integer getTyp() {
        return typ;
    }

    public void setTyp(Integer typ) {
        this.typ = typ;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres_woj() {
        return adres_woj;
    }

    public void setAdres_woj(String adres_woj) {
        this.adres_woj = adres_woj;
    }

    public String getAdres_kod() {
        return adres_kod;
    }

    public void setAdres_kod(String adres_kod) {
        this.adres_kod = adres_kod;
    }
}
