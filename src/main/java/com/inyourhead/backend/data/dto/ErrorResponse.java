package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 04.12.16.
 */
public class ErrorResponse {

    private Integer httpStatusCode;

    private String description;

    public ErrorResponse() {

    }

    public ErrorResponse(Integer httpStatusCode, String description) {
        this.httpStatusCode = httpStatusCode;
        this.description = description;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
