package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 29.12.16.
 */
public class ZapytanieDTO {

    private String fraza;
    private Double cena_min;
    private Double cena_max;
    private Integer id_kat;

    private Double lat;
    private Double lng;

    public ZapytanieDTO() {

    }

    public ZapytanieDTO(String fraza, Double cena_min, Double cena_max, Integer id_kat, Double lat, Double lng) {
        this.fraza = fraza;
        this.cena_min = cena_min;
        this.cena_max = cena_max;
        this.id_kat = id_kat;
        this.lat = lat;
        this.lng = lng;
    }

    public String getFraza() {
        return fraza;
    }

    public void setFraza(String fraza) {
        this.fraza = fraza;
    }

    public Double getCena_min() {
        return cena_min;
    }

    public void setCena_min(Double cena_min) {
        this.cena_min = cena_min;
    }

    public Double getCena_max() {
        return cena_max;
    }

    public void setCena_max(Double cena_max) {
        this.cena_max = cena_max;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
