package com.inyourhead.backend.data.dto;

import com.inyourhead.backend.utils.ImageBase64Converter;

/**
 * Created by aleksander on 26.12.16.
 */
public class ProduktDTO {


    private Integer id_prod_szczeg;
    private Integer id_prod;
    private Integer id_sklep;
    private String kat_nazwa;
    private String nazwa;
    private Double cena;
    private String opis;
    private String zdjecie;

    public ProduktDTO() {

    }

    public ProduktDTO(Integer id_prod_szczeg, Integer id_prod, Integer id_sklep, String kat_nazwa, String nazwa, Double cena, String opis, String zdjecie) {
        this.id_prod_szczeg = id_prod_szczeg;
        this.id_prod = id_prod;
        this.id_sklep = id_sklep;
        this.kat_nazwa = kat_nazwa;
        this.nazwa = nazwa;
        this.cena = cena;
        this.opis = opis;
        this.zdjecie = zdjecie;
    }

    public ProduktDTO(Integer id_prod_szczeg, Integer id_prod, Integer id_sklep, String kat_nazwa, String nazwa, Double cena, String opis, byte[] zdjecie) {
        this.id_prod_szczeg = id_prod_szczeg;
        this.id_prod = id_prod;
        this.id_sklep = id_sklep;
        this.kat_nazwa = kat_nazwa;
        this.nazwa = nazwa;
        this.cena = cena;
        this.opis = opis;
        this.zdjecie = ImageBase64Converter.encodeToString(zdjecie);
    }

    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }

    public Integer getId_sklep() {
        return id_sklep;
    }

    public void setId_sklep(Integer id_sklep) {
        this.id_sklep = id_sklep;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public Integer getId_prod_szczeg() {
        return id_prod_szczeg;
    }

    public void setId_prod_szczeg(Integer id_prod_szczeg) {
        this.id_prod_szczeg = id_prod_szczeg;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

}
