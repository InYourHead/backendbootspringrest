package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 04.04.17.
 */
public class DefaultProductDTO {

    private Integer id_prod;
    private String nazwa;
    private String opis;
    private String zdjecie;
    private Integer id_kat;

    public DefaultProductDTO() {
        super();
    }

    public DefaultProductDTO(Integer id_prod, String nazwa, String opis, String zdjecie, Integer id_kat) {
        this.id_prod = id_prod;
        this.nazwa = nazwa;
        this.opis = opis;
        this.zdjecie = zdjecie;
        this.id_kat = id_kat;
    }

    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }
}
