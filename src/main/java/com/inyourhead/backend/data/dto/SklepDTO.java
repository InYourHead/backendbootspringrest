package com.inyourhead.backend.data.dto;

import com.inyourhead.backend.data.entities.Sklep;
import com.inyourhead.backend.utils.ImageBase64Converter;

/**
 * Created by aleksander on 04.12.16.
 */
public class SklepDTO {

    private String adres_kod = null;

    private String adres_miasto = null;

    private String adres_nr_domu = null;

    private String adres_ulica = null;

    private String adres_woj = null;

    private Integer id_sklep = null;

    private String nazwa = null;

    private String zdjecie = null;

    private String facebook;

    private String www;

    private Double lng;

    private Double lat;

    private Integer id_kat;

    private String kat_nazwa;

    public SklepDTO() {

    }

    public SklepDTO(String adres_kod, String adres_miasto, String adres_nr_domu, String adres_ulica, String adres_woj, Integer id_sklep, String nazwa, String zdjecie, String facebook, String www, Double lng, Double lat, Integer id_kat, String kat_nazwa) {
        this.adres_kod = adres_kod;
        this.adres_miasto = adres_miasto;
        this.adres_nr_domu = adres_nr_domu;
        this.adres_ulica = adres_ulica;
        this.adres_woj = adres_woj;
        this.id_sklep = id_sklep;
        this.nazwa = nazwa;
        this.zdjecie = zdjecie;
        this.facebook = facebook;
        this.www = www;
        this.lng = lng;
        this.lat = lat;
        this.id_kat = id_kat;
        this.kat_nazwa = kat_nazwa;
    }

    public SklepDTO(String adres_kod, String adres_miasto, String adres_nr_domu, String adres_ulica, String adres_woj, Integer id_sklep, String nazwa, byte[] zdjecie, String facebook, String www, Double lng, Double lat, Integer id_kat, String kat_nazwa) {
        this.adres_kod = adres_kod;
        this.adres_miasto = adres_miasto;
        this.adres_nr_domu = adres_nr_domu;
        this.adres_ulica = adres_ulica;
        this.adres_woj = adres_woj;
        this.id_sklep = id_sklep;
        this.nazwa = nazwa;
        this.zdjecie = ImageBase64Converter.encodeToString(zdjecie);
        this.facebook = facebook;
        this.www = www;
        this.lng = lng;
        this.lat = lat;
        this.id_kat = id_kat;
        this.kat_nazwa = kat_nazwa;
    }

    public String getAdres_kod() {
        return adres_kod;
    }

    public void setAdres_kod(String adres_kod) {
        this.adres_kod = adres_kod;
    }

    public String getAdres_miasto() {
        return adres_miasto;
    }

    public void setAdres_miasto(String adres_miasto) {
        this.adres_miasto = adres_miasto;
    }

    public String getAdres_nr_domu() {
        return adres_nr_domu;
    }

    public void setAdres_nr_domu(String adres_nr_domu) {
        this.adres_nr_domu = adres_nr_domu;
    }

    public String getAdres_ulica() {
        return adres_ulica;
    }

    public void setAdres_ulica(String adres_ulica) {
        this.adres_ulica = adres_ulica;
    }

    public String getAdres_woj() {
        return adres_woj;
    }

    public void setAdres_woj(String adres_woj) {
        this.adres_woj = adres_woj;
    }

    public Integer getId_sklep() {
        return id_sklep;
    }

    public void setId_sklep(Integer id_sklep) {
        this.id_sklep = id_sklep;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public Sklep toSklep() {

        Sklep sklep = new Sklep();

        sklep.setIdSklep(this.id_sklep);
        sklep.setNazwa(this.nazwa);
        sklep.setUzytkownikIdUser(null);
        sklep.setAdresKod(this.adres_kod);
        sklep.setAdresMiasto(this.adres_miasto);
        sklep.setAdresNrDomu(this.adres_nr_domu);
        sklep.setAdresUlica(this.adres_ulica);
        sklep.setAdresWoj(this.adres_woj);
        sklep.setZdjecie(ImageBase64Converter.decodeToByteArray(this.zdjecie));
        sklep.setFacebook(this.facebook);
        sklep.setWww(this.www);
        sklep.setWspolX(this.lng);
        sklep.setWspolY(this.lat);
        return sklep;
    }

}
