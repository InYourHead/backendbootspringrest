package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 25.12.16.
 */
public class HistoriaDTO {


    private Integer nr;

    private String fraza;

    private Double cena_min;

    private Double cena_max;

    private Integer id_kat;

    private long data;

    public HistoriaDTO() {

    }

    public HistoriaDTO(Integer nr, String fraza, Double cena_min, Double cena_max, Integer id_kat, long data) {
        this.nr = nr;
        this.fraza = fraza;
        this.cena_min = cena_min;
        this.cena_max = cena_max;
        this.id_kat = id_kat;
        this.data = data;
    }

    public String getFraza() {
        return fraza;
    }

    public void setFraza(String fraza) {
        this.fraza = fraza;
    }

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public Double getCena_min() {
        return cena_min;
    }

    public void setCena_min(Double cena_min) {
        this.cena_min = cena_min;
    }

    public Double getCena_max() {
        return cena_max;
    }

    public void setCena_max(Double cena_max) {
        this.cena_max = cena_max;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }
}
