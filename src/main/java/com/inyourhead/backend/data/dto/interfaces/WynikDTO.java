package com.inyourhead.backend.data.dto.interfaces;

/**
 * Created by aleksander on 29.12.16.
 */
public interface WynikDTO {

    Double getX();

    void setX(Double x);

    Double getY();

    void setY(Double y);
}
