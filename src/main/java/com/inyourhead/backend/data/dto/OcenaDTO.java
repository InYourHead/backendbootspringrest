package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 06.04.17.
 */
public class OcenaDTO {

    private Integer ocena;

    public OcenaDTO() {
        super();
    }

    public OcenaDTO(Integer ocena) {
        this.ocena = ocena;
    }

    public Integer getOcena() {
        return ocena;
    }

    public void setOcena(Integer ocena) {
        this.ocena = ocena;
    }
}
