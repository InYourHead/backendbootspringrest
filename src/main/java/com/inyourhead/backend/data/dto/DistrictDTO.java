package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 18.03.17.
 */
public class DistrictDTO {


    private Integer id;
    private String name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DistrictDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }


}
