package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 12.04.17.
 */
public class NewPasswordDTO {

    private String newPassword;

    public NewPasswordDTO() {
        super();
    }

    public NewPasswordDTO(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
