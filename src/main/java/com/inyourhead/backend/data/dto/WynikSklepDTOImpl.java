package com.inyourhead.backend.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;

/**
 * Created by aleksander on 29.12.16.
 */
public class WynikSklepDTOImpl implements WynikDTO {

    private Integer id_kat;
    private String kat_nazwa;
    private Integer id_sklep;
    private String facebook;
    private String www;
    private String nazwa;
    private String adres_ulica;
    private String adres_nr_domu;
    private String adres_kod;
    private String adres_miasto;
    private String adres_woj;
    private byte[] zdjecie;
    private Double lng;
    private Double lat;

    public WynikSklepDTOImpl() {
        super();
    }

    public WynikSklepDTOImpl(Integer id_kat, String kat_nazwa, Integer id_sklep, String facebook, String www, String nazwa, String adres_ulica, String adres_nr_domu, String adres_kod, String adres_miasto, String adres_woj, byte[] zdjecie, Double lng, Double lat) {
        this.id_kat = id_kat;
        this.kat_nazwa = kat_nazwa;
        this.id_sklep = id_sklep;
        this.facebook = facebook;
        this.www = www;
        this.nazwa = nazwa;
        this.adres_ulica = adres_ulica;
        this.adres_nr_domu = adres_nr_domu;
        this.adres_kod = adres_kod;
        this.adres_miasto = adres_miasto;
        this.adres_woj = adres_woj;
        this.zdjecie = zdjecie;
        this.lng = lng;
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public Integer getId_sklep() {
        return id_sklep;
    }

    public void setId_sklep(Integer id_sklep) {
        this.id_sklep = id_sklep;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres_ulica() {
        return adres_ulica;
    }

    public void setAdres_ulica(String adres_ulica) {
        this.adres_ulica = adres_ulica;
    }

    public String getAdres_nr_domu() {
        return adres_nr_domu;
    }

    public void setAdres_nr_domu(String adres_nr_domu) {
        this.adres_nr_domu = adres_nr_domu;
    }

    public String getAdres_kod() {
        return adres_kod;
    }

    public void setAdres_kod(String adres_kod) {
        this.adres_kod = adres_kod;
    }

    public String getAdres_miasto() {
        return adres_miasto;
    }

    public void setAdres_miasto(String adres_miasto) {
        this.adres_miasto = adres_miasto;
    }

    public String getAdres_woj() {
        return adres_woj;
    }

    public void setAdres_woj(String adres_woj) {
        this.adres_woj = adres_woj;
    }

    public byte[] getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }


    @JsonIgnore
    @Override
    public Double getX() {
        return lng;
    }

    @JsonIgnore
    @Override
    public void setX(Double x) {
        this.lng = x;
    }

    @JsonIgnore
    @Override
    public Double getY() {
        return lat;
    }

    @JsonIgnore
    @Override
    public void setY(Double y) {
        this.lat = y;
    }
}
