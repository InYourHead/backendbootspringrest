package com.inyourhead.backend.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;

/**
 * Created by aleksander on 09.01.17.
 */
public class WynikProduktObiektDTOImpl implements WynikDTO, Comparable<WynikProduktObiektDTOImpl> {



    private Integer id_kat;
    private String kat_nazwa;
    private Integer id_prod;
    private Double ocena;
    private String nazwa;
    private String opis;
    private Double cena;
    private byte[] zdjecie;
    private WynikSklepDTOImpl sklep;

    public WynikProduktObiektDTOImpl(Integer id_kat, String kat_nazwa, Integer id_prod, Double ocena, String nazwa, String opis, Double cena, byte[] zdjecie, WynikSklepDTOImpl sklep) {
        this.id_kat = id_kat;
        this.kat_nazwa = kat_nazwa;
        this.id_prod = id_prod;
        this.ocena = ocena;
        this.nazwa = nazwa;
        this.opis = opis;
        this.cena = cena;
        this.zdjecie = zdjecie;
        this.sklep = sklep;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }

    public Double getOcena() {
        return ocena;
    }

    public void setOcena(Double ocena) {
        this.ocena = ocena;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public byte[] getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }

    public WynikSklepDTOImpl getSklep() {
        return sklep;
    }

    public void setSklep(WynikSklepDTOImpl sklep) {
        this.sklep = sklep;
    }

    @JsonIgnore
    @Override
    public Double getX() {
        return sklep.getX();
    }

    @JsonIgnore
    @Override
    public void setX(Double x) {
        sklep.setX(x);
    }

    @JsonIgnore
    public Double getY() {
        return sklep.getY();
    }

    @JsonIgnore
    public void setY(Double y) {
        sklep.setY(y);
    }

    @JsonIgnore
    @Override
    public int compareTo(WynikProduktObiektDTOImpl o) {
        return this.getNazwa().compareTo(o.getNazwa());
    }
}
