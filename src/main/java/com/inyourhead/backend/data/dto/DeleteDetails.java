package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 04.12.16.
 */
public class DeleteDetails {

    private Integer deleteCount;


    public DeleteDetails(Integer deleteCount) {
        this.deleteCount = deleteCount;
    }

    public Integer getDeleteCount() {
        return deleteCount;
    }

    public void setDeleteCount(Integer deleteCount) {
        this.deleteCount = deleteCount;
    }
}
