package com.inyourhead.backend.data.dto;

import com.inyourhead.backend.data.entities.Promocja;

/**
 * Created by aleksander on 28.12.16.
 */
public class PromocjaDTO {

    private Integer id_prod;

    private Double cena_promocyjna;

    public PromocjaDTO() {

    }

    public PromocjaDTO(Integer id_prod, Double cena_promocyjna) {
        this.id_prod = id_prod;
        this.cena_promocyjna = cena_promocyjna;
    }

    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }

    public Double getCena_promocyjna() {
        return cena_promocyjna;
    }

    public void setCena_promocyjna(Double cena_promocyjna) {
        this.cena_promocyjna = cena_promocyjna;
    }

    public Promocja toPromocja() {
        Promocja result = new Promocja();

        result.setCenaPromocyjna(this.cena_promocyjna);

        return result;
    }
}
