package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 03.01.17.
 */
public class TokenDTO {

    private String token;

    private String device_id;

    public TokenDTO() {

    }

    public TokenDTO(String token, String device_id) {
        this.token = token;
        this.device_id = device_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
