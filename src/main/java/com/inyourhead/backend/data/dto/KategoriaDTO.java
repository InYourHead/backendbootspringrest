package com.inyourhead.backend.data.dto;

/**
 * Created by aleksander on 30.12.16.
 */
public class KategoriaDTO {

    private Integer id_kat;
    private String nazwa;

    public KategoriaDTO() {

    }

    public KategoriaDTO(Integer id_kat, String nazwa) {
        this.id_kat = id_kat;
        this.nazwa = nazwa;
    }

    public Integer getId_kat() {
        return id_kat;
    }

    public void setId_kat(Integer id_kat) {
        this.id_kat = id_kat;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
