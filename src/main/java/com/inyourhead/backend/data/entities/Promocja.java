package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.PromocjaDTO;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by aleksander on 23.12.16.
 */
@Entity
@Table(name = "promocja")
public class Promocja implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4444301535157428614L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_prom")
    private Integer idProm;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_prod", referencedColumnName = "id_prod_szczeg")
    private ProduktSzczegoly idProd;

    @Column(scale = 2, name = "cena")
    private Double cenaPromocyjna;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_sklep", referencedColumnName = "id_sklep")
    private Sklep idSklep;


    public Promocja() {
        super();
    }

    public Promocja(ProduktSzczegoly idProd, Double cenaPromocyjna, Sklep idSklep) {
        this.idProd = idProd;
        this.cenaPromocyjna = cenaPromocyjna;
        this.idSklep = idSklep;
    }

    public Integer getIdProm() {
        return idProm;
    }

    public void setIdProm(Integer idProm) {
        this.idProm = idProm;
    }

    public ProduktSzczegoly getIdProd() {
        return idProd;
    }

    public void setIdProd(ProduktSzczegoly idProd) {
        this.idProd = idProd;
    }

    public Double getCenaPromocyjna() {
        return cenaPromocyjna;
    }

    public void setCenaPromocyjna(Double cenaPromocyjna) {
        this.cenaPromocyjna = cenaPromocyjna;
    }

    public Sklep getIdSklep() {
        return idSklep;
    }

    public void setIdSklep(Sklep idSklep) {
        this.idSklep = idSklep;
    }

    public PromocjaDTO toPromocjaDTO() {
        PromocjaDTO result = new PromocjaDTO();

        result.setId_prod(this.idProd.getIdProdSzczeg());
        result.setCena_promocyjna(this.cenaPromocyjna);

        return result;
    }
}
