/**
 *
 */
package com.inyourhead.backend.data.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author aleksander
 *
 */
@Entity
@Table(name = "produkt")
public class Produkt implements Serializable
{

	/**
	 *
	 */
	private static final long serialVersionUID = 4444301535157428614L;



	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_prod")
	private Integer idProd;

	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "id_kat", referencedColumnName = "id_kat")
	private Kategoria kategoriaIdKat;

	@Column
	@Size(max = 100)
	private String nazwa;

	@Column
	@Size(max = 1000)
	private String opis;

    @Column
    private byte[] zdjecie;

	/**
	 *
	 */
	public Produkt()
	{
		super();
	}

    public Produkt(Kategoria kategoriaIdKat, String nazwa, String opis, byte[] zdjecie) {
        this.kategoriaIdKat = kategoriaIdKat;
        this.nazwa = nazwa;
		this.opis = opis;
		this.zdjecie = zdjecie;
	}

	public Integer getIdProd() {
		return idProd;
	}

	public void setIdProd(Integer idProd) {
		this.idProd = idProd;
	}

	public Kategoria getKategoriaIdKat() {
		return kategoriaIdKat;
	}

	public void setKategoriaIdKat(Kategoria kategoriaIdKat) {
		this.kategoriaIdKat = kategoriaIdKat;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}


	public byte[] getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(byte[] zdjecie) {
		this.zdjecie = zdjecie;
	}

}
