/**
 *
 */
package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.HistoriaDTO;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author aleksander
 *
 */
@Entity
@Table(name = "historia")
@SuppressWarnings("javadoc")
public class Historia implements Serializable
{

	/**
	 *
	 */
	private static final long serialVersionUID = 8142021681652382786L;

	@Column
	@Size(max = 50)
	private String fraza;

	@Column(name = "id_kat")
	private Integer idKat;

	@Column(name = "cena_min")
	private Double cenaMin;

	@Column(name = "cena_max")
	private Double cenaMax;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

	@Column
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer nr;

	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "id_user", referencedColumnName ="id_user" )
	private Uzytkownik uzytkownikIdUser;

	/**
	 *
	 */
	public Historia()
	{
		super();
	}

	public Historia(String fraza, Integer idKat, Double cenaMin, Double cenaMax) {
		this.fraza = fraza;
		this.idKat = idKat;
		this.cenaMin = cenaMin;
		this.cenaMax = cenaMax;
	}

	/**
	 * @return the fraza
	 */
	public String getFraza()
	{
		return fraza;
	}

	/**
	 * @param fraza
	 *                the fraza to set
	 */
	public void setFraza(String fraza)
	{
		this.fraza = fraza;
	}

	/**
	 * @return the nr
	 */
	public Integer getNr() {
		return nr;
	}

	/**
	 * @param nr
	 *                the nr to set
	 */
	public void setNr(Integer nr)
	{
		this.nr = nr;
	}

	/**
	 * @return the uzytkownikIdUser
	 */
	public Uzytkownik getUzytkownikIdUser() {
		return uzytkownikIdUser;
	}

	/**
	 * @param uzytkownikIdUser
	 *                the uzytkownikIdUser to set
	 */
	public void setUzytkownikIdUser(Uzytkownik uzytkownikIdUser)
	{
		this.uzytkownikIdUser = uzytkownikIdUser;
	}


	public Integer getIdKat() {
		return idKat;
	}

	public void setIdKat(Integer idKat) {
		this.idKat = idKat;
	}

	public Double getCenaMin() {
		return cenaMin;
	}

	public void setCenaMin(Double cenaMin) {
		this.cenaMin = cenaMin;
	}

	public Double getCenaMax() {
		return cenaMax;
	}

	public void setCenaMax(Double cenaMax) {
		this.cenaMax = cenaMax;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public HistoriaDTO toHistoriaDTO() {
		HistoriaDTO result = new HistoriaDTO();

		result.setFraza(this.fraza);
		result.setNr(this.nr);
		result.setCena_min(this.cenaMin);
		result.setCena_max(this.cenaMax);
		result.setId_kat(this.idKat);
		result.setData(this.data.getTime() / 1000);
		return result;

	}

}
