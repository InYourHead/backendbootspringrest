package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.entities.primaryKeys.OcenyProduktPK;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by aleksander on 01.04.17.
 */
@Entity
@Table(name = "oceny_prod")
@IdClass(OcenyProduktPK.class)
public class OcenyProdukt {

    @Id
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_prod_szczeg", referencedColumnName = "id_prod_szczeg")
    private ProduktSzczegoly idProd;

    @Id
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_user", referencedColumnName = "id_user")
    private Uzytkownik idUser;

    @Column(name = "ocena")
    @Max(5)
    @Min(1)
    private Integer ocena;

    public OcenyProdukt() {
        super();
    }

    public OcenyProdukt(ProduktSzczegoly idProd, Uzytkownik idUser, Integer ocena) {
        this.idProd = idProd;
        this.idUser = idUser;
        this.ocena = ocena;
    }

    public ProduktSzczegoly getIdProd() {
        return idProd;
    }

    public void setIdProd(ProduktSzczegoly idProd) {
        this.idProd = idProd;
    }

    public Uzytkownik getIdUser() {
        return idUser;
    }

    public void setIdUser(Uzytkownik idUser) {
        this.idUser = idUser;
    }

    public Integer getOcena() {
        return ocena;
    }

    public void setOcena(Integer ocena) {
        this.ocena = ocena;
    }
}
