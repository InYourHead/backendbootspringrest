package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.UzytkownikDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@SuppressWarnings("javadoc")
@Entity
@Table(name = "uzytkownik")
public class Uzytkownik implements Serializable
{

	/**
	 *
	 */
	private static final long serialVersionUID = -6583393971736224029L;

	@Size(max = 6)
	@Column(name = "adres_kod")
	private String adresKod;

	@Size(max = 20)
	@Column(name = "adres_miasto")
	private String adresMiasto;

	@Size(max = 4)
	@Column(name = "adres_nr_domu")
	private String adresNrDomu;

	@Size(max = 20)
	@Column(name = "adres_ulica")
	private String adresUlica;

	@Size(max = 40)
	@Column(name = "adres_woj")
	private String adresWoj;

    @Column(unique = true)
    @Size(max = 100)
	private String email;

	@Column
	private String haslo;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user")
	private Integer idUser;

	@Column
	@Size(max = 20)
	private String imie;

    @Column(unique = true)
    @Size(max = 30)
	private String login;

	@Column
	@Size(max = 30)
	private String nazwisko;


	@Column
	@Size(max = 13)
	private String telefon;

	@Column
	@NotNull
	private Integer typ;

	/**
	 *
	 */
	public Uzytkownik()
	{
	}

	/**
	 * @param idUser
	 * @param typ
	 * @param imie
	 * @param nazwisko
	 * @param email
	 * @param telefon
	 * @param adresUlica
	 * @param adresNrDomu
	 * @param adresKod
	 * @param adresMiasto
	 * @param adresWoj
	 * @param haslo
	 * @param login
	 */

	public Uzytkownik(Integer idUser, Integer typ, String imie,
		String nazwisko, String email, String telefon,
		String adresUlica, String adresNrDomu, String adresKod,
		String adresMiasto, String adresWoj, String haslo, String login)
	{
		this.idUser = idUser;
		this.typ = typ;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.email = email;
		this.telefon = telefon;
		this.adresUlica = adresUlica;
		this.adresNrDomu = adresNrDomu;
		this.adresKod = adresKod;
		this.adresMiasto = adresMiasto;
		this.adresWoj = adresWoj;
		this.haslo = haslo;
		this.login = login;
	}

	/**
	 * @return the adresKod
	 */
	public String getAdresKod()
	{
		return adresKod;
	}

	/**
	 * @param adresKod
	 *                the adresKod to set
	 */
	public void setAdresKod(String adresKod)
	{
		this.adresKod = adresKod;
	}

	/**
	 * @return the adresMiasto
	 */
	public String getAdresMiasto() {
		return adresMiasto;
	}

	/**
	 * @param adresMiasto
	 *                the adresMiasto to set
	 */
	public void setAdresMiasto(String adresMiasto)
	{
		this.adresMiasto = adresMiasto;
	}

	/**
	 * @return the adresNrDomu
	 */
	public String getAdresNrDomu() {
		return adresNrDomu;
	}

	/**
	 * @param adresNrDomu
	 *                the adresNrDomu to set
	 */
	public void setAdresNrDomu(String adresNrDomu)
	{
		this.adresNrDomu = adresNrDomu;
	}

	/**
	 * @return the adresUlica
	 */
	public String getAdresUlica() {
		return adresUlica;
	}

	/**
	 * @param adresUlica
	 *                the adresUlica to set
	 */
	public void setAdresUlica(String adresUlica)
	{
		this.adresUlica = adresUlica;
	}

	/**
	 * @return the adresWoj
	 */
	public String getAdresWoj() {
		return adresWoj;
	}

	/**
	 * @param adresWoj
	 *                the adresWoj to set
	 */
	public void setAdresWoj(String adresWoj)
	{
		this.adresWoj = adresWoj;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *                the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the haslo
	 */
	public String getHaslo() {
		return haslo;
	}

	/**
	 * @param haslo
	 *                the haslo to set
	 */
	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	/**
	 * @return the idUser
	 */
	public Integer getIdUser() {
		return idUser;
	}

	/**
	 * @param idUser
	 *                the idUser to set
	 */
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	/**
	 * @return the imie
	 */
	public String getImie() {
		return imie;
	}

	/**
	 * @param imie
	 *                the imie to set
	 */
	public void setImie(String imie) {
		this.imie = imie;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *                the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the nazwisko
	 */
	public String getNazwisko() {
		return nazwisko;
	}

	/**
	 * @param nazwisko
	 *                the nazwisko to set
	 */
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	/**
	 * @return the telefon
	 */
	public String getTelefon()
	{
		return telefon;
	}

	/**
	 * @param telefon
	 *                the telefon to set
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	/**
	 * @param sklep
	 *                the sklep to set
	 */

	/**
	 * @return the typ
	 */
	public Integer getTyp() {
		return typ;
	}

	/**
	 * @param typ
	 *                the typ to set
	 */
	public void setTyp(Integer typ) {
		this.typ = typ;
	}

	public UzytkownikDTO toUzytkownikDTO() {

		return new UzytkownikDTO(this);

	}
}
