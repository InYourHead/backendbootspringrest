package com.inyourhead.backend.data.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by aleksander on 01.04.17.
 */
@Entity
@Table(name = "prod_szczeg")
public class ProduktSzczegoly {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_prod_szczeg")
    private Integer idProdSzczeg;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_prod", referencedColumnName = "id_prod")
    private Produkt idProd;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_sklep", referencedColumnName = "id_sklep")
    private Sklep sklepIdSklep;

    @Column
    @Size(max = 100)
    private String nazwa;

    @Column
    private Double cena;

    @Column
    @Size(max = 1000)
    private String opis;

    @Column
    private byte[] zdjecie;

    public ProduktSzczegoly() {
        super();
    }

    public ProduktSzczegoly(Produkt idProd, Sklep sklepIdSklep, String nazwa, Double cena, String opis, byte[] zdjecie) {
        this.idProd = idProd;
        this.sklepIdSklep = sklepIdSklep;
        this.nazwa = nazwa;
        this.cena = cena;
        this.opis = opis;
        this.zdjecie = zdjecie;
    }

    public Integer getIdProdSzczeg() {
        return idProdSzczeg;
    }

    public void setIdProdSzczeg(Integer idProdSzczeg) {
        this.idProdSzczeg = idProdSzczeg;
    }

    public Produkt getIdProd() {
        return idProd;
    }

    public void setIdProd(Produkt idProd) {
        this.idProd = idProd;
    }

    public Sklep getSklepIdSklep() {
        return sklepIdSklep;
    }

    public void setSklepIdSklep(Sklep sklepIdSklep) {
        this.sklepIdSklep = sklepIdSklep;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public byte[] getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }
}
