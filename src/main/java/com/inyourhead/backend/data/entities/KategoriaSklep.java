package com.inyourhead.backend.data.entities;

import javax.persistence.*;

/**
 * Created by aleksander on 01.04.17.
 */
@Entity
@Table(name = "kat_sklep")
public class KategoriaSklep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_kat_sklep")
    private Integer idKatSklep;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_sklep", referencedColumnName = "id_sklep")
    private Sklep idSklep;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "id_kat", referencedColumnName = "id_kat")
    private Kategoria idKat;

    public KategoriaSklep() {
        super();
    }

    public KategoriaSklep(Sklep idSklep, Kategoria idKat) {
        this.idSklep = idSklep;
        this.idKat = idKat;
    }

    public Integer getIdKatSklep() {
        return idKatSklep;
    }

    public void setIdKatSklep(Integer idKatSklep) {
        this.idKatSklep = idKatSklep;
    }

    public Sklep getIdSklep() {
        return idSklep;
    }

    public void setIdSklep(Sklep idSklep) {
        this.idSklep = idSklep;
    }

    public Kategoria getIdKat() {
        return idKat;
    }

    public void setIdKat(Kategoria idKat) {
        this.idKat = idKat;
    }
}
