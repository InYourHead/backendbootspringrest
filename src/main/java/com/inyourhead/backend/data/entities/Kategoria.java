/**
 *
 */
package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.KategoriaDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author aleksander
 *
 */
@Entity
@Table(name = "kategoria")
@SuppressWarnings("javadoc")
public class Kategoria implements Serializable
{

	/**
	 *
	 */
	private static final long serialVersionUID = 4730649762401943695L;

	@Column(name = "id_kat")
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idKat;

	@Column
	@Size(max = 30)
	private String nazwa;

	/**
	 *
	 */
	public Kategoria()
	{
		super();
	}

	/**
	 * @param idKat
	 * @param nazwa
	 */
	public Kategoria(Integer idKat, String nazwa)
	{
		super();
		this.idKat = idKat;
		this.nazwa = nazwa;
	}

	/**
	 * @return the idKat
	 */
	public Integer getIdKat()
	{
		return idKat;
	}

	/**
	 * @param idKat
	 *                the idKat to set
	 */
	public void setIdKat(Integer idKat)
	{
		this.idKat = idKat;
	}

	/**
	 * @return the nazwa
	 */
	public String getNazwa() {
		return nazwa;
	}

	/**
	 * @param nazwa
	 *                the nazwa to set
	 */
	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}

	public KategoriaDTO toKategoriaDTO() {
		return new KategoriaDTO(this.idKat, this.nazwa);
	}
}
