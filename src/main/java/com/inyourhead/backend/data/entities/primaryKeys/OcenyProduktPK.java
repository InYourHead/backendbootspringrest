package com.inyourhead.backend.data.entities.primaryKeys;

import java.io.Serializable;

/**
 * Created by aleksander on 01.04.17.
 */
public class OcenyProduktPK implements Serializable {

    protected Integer idProd;
    protected Integer idUser;

    public OcenyProduktPK() {
    }

    public OcenyProduktPK(Integer id_prod, Integer id_user) {
        this.idProd = id_prod;
        this.idUser = id_user;
    }
}
