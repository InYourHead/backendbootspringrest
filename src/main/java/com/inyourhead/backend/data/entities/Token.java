package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.TokenDTO;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by aleksander on 03.01.17.
 */
@Entity
@Table(name = "token")
public class Token {


    @Column
    @Id
    @NotNull
    private String token;

    @Column(name = "singing_key")
    private String singingKey;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "create_date")
    @Type(type = "date")
    private Date createDate;

    public Token() {
        this.createDate = new Date();
    }

    public Token(String token, String singingKey, String deviceId, Integer userId) {
        this.token = token;
        this.singingKey = singingKey;
        this.deviceId = deviceId;
        this.userId = userId;
        this.createDate = new Date();
    }

    public Token(String token, String singingKey, Integer userId) {
        this.token = token;
        this.singingKey = singingKey;
        this.userId = userId;
        this.createDate = new Date();
        this.deviceId = "RESET_PASSWORD";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSingingKey() {
        return singingKey;
    }

    public void setSingingKey(String singingKey) {
        this.singingKey = singingKey;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TokenDTO toTokenDTO() {
        TokenDTO result = new TokenDTO();

        result.setToken(this.token);

        result.setDevice_id(this.deviceId);

        return result;
    }
}
