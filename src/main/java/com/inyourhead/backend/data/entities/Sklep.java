/**
 *
 */
package com.inyourhead.backend.data.entities;

import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.dto.WynikSklepDTOImpl;
import com.inyourhead.backend.utils.ImageBase64Converter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author aleksander
 *
 */
@Entity
@Table(name = "sklep")
@SuppressWarnings("javadoc")
public class Sklep implements Serializable
{

	/**
	 *

	 */
	private static final long serialVersionUID = 7223126184208605297L;
	@Size(max = 6)
	@Column(name = "adres_kod")
	private String adresKod;
	@Size(max = 20)
	@Column(name = "adres_miasto")
	private String adresMiasto;
	@Size(max = 4)
	@Column(name = "adres_nr_domu")
	private String adresNrDomu;
	@Size(max = 20)
	@Column(name = "adres_ulica")
	private String adresUlica;
	@Size(max = 40)
	@Column(name = "adres_woj")
	private String adresWoj;
	@Column(name = "id_sklep")
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idSklep;
	@Column
	@Size(max = 100)
	private String nazwa;
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "id_user", referencedColumnName = "id_user")
	private Uzytkownik uzytkownikIdUser;
    @Column
    private byte[] zdjecie;
    @Column
	@Size(max = 200)
	private String facebook;
	@Column
	@Size(max = 100)
	private String www;
	@Column(name = "wspol_x")
    private Double wspolX;
    @Column(name = "wspol_y")
    private Double wspolY;

	/**
	 *
	 */
	public Sklep()
	{
		super();
	}

    public Sklep(String adresKod, String adresMiasto, String adresNrDomu, String adresUlica, String adresWoj, String nazwa, Uzytkownik uzytkownikIdUser, byte[] zdjecie, String facebook, String www, Double wspol_x, Double wspol_y) {
        this.adresKod = adresKod;
		this.adresMiasto = adresMiasto;
		this.adresNrDomu = adresNrDomu;
		this.adresUlica = adresUlica;
		this.adresWoj = adresWoj;
		this.nazwa = nazwa;
		this.uzytkownikIdUser = uzytkownikIdUser;
		this.zdjecie = zdjecie;
		this.facebook = facebook;
		this.www = www;
		this.wspolX = wspol_x;
		this.wspolY = wspol_y;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

    public Double getWspolX() {
        return wspolX;
	}

    public void setWspolX(Double wspol_x) {
        this.wspolX = wspol_x;
	}

    public Double getWspolY() {
        return wspolY;
	}

    public void setWspolY(Double wspol_y) {
        this.wspolY = wspol_y;
	}

    public Uzytkownik getUzytkownikIdUser() {
        return uzytkownikIdUser;
    }

    public void setUzytkownikIdUser(Uzytkownik uzytkownikIdUser) {
        this.uzytkownikIdUser = uzytkownikIdUser;
    }

    public SklepDTO toSklepDTO() {
        SklepDTO sklep = new SklepDTO();

		sklep.setId_sklep(this.idSklep);
		sklep.setNazwa(this.nazwa);
		sklep.setAdres_kod(this.adresKod);
		sklep.setAdres_miasto(this.adresMiasto);
		sklep.setAdres_nr_domu(this.adresNrDomu);
		sklep.setAdres_ulica(this.adresUlica);
		sklep.setAdres_woj(this.adresWoj);
        sklep.setZdjecie(ImageBase64Converter.encodeToString(this.zdjecie));
        sklep.setFacebook(this.facebook);
		sklep.setWww(this.www);
		sklep.setLng(this.wspolX);
		sklep.setLat(this.wspolY);
		return sklep;
	}

	public WynikSklepDTOImpl toWynikSklepDTO() {
		WynikSklepDTOImpl result = new WynikSklepDTOImpl();

		result.setId_sklep(this.idSklep);
		result.setNazwa(this.nazwa);
		result.setAdres_kod(this.adresKod);
		result.setAdres_miasto(this.adresMiasto);
		result.setAdres_nr_domu(this.adresNrDomu);
		result.setAdres_ulica(this.adresUlica);
		result.setAdres_woj(this.adresWoj);
		result.setZdjecie(this.zdjecie);
		result.setFacebook(this.facebook);
		result.setWww(this.www);
        result.setX(this.wspolX);
        result.setY(this.wspolY);

		return result;
	}

	/**
	 * @return the adresKod
	 */
	public String getAdresKod()
	{
		return adresKod;
	}

    public void setAdresKod(String adresKod) {
        this.adresKod = adresKod;
    }

	/**
	 * @return the adresMiasto
	 */
	public String getAdresMiasto()
	{
		return adresMiasto;
	}

    public void setAdresMiasto(String adresMiasto) {
        this.adresMiasto = adresMiasto;
    }

	/**
	 * @return the adresNrDomu
	 */
	public String getAdresNrDomu()
	{
		return adresNrDomu;
	}

    public void setAdresNrDomu(String adresNrDomu) {
        this.adresNrDomu = adresNrDomu;
    }

	/**
	 * @return the adresUlica
	 */
	public String getAdresUlica()
	{
		return adresUlica;
	}

    public void setAdresUlica(String adresUlica) {
        this.adresUlica = adresUlica;
    }

	/**
	 * @return the adresWoj
	 */
	public String getAdresWoj()
	{
		return adresWoj;
	}

    public void setAdresWoj(String adresWoj) {
        this.adresWoj = adresWoj;
    }

	/**
	 * @return the idSklep
	 */
	public Integer getIdSklep()
	{
		return idSklep;
	}

    public void setIdSklep(Integer idSklep) {
        this.idSklep = idSklep;
    }

	/**
	 * @return the nazwa
	 */
	public String getNazwa()
	{
		return nazwa;
	}

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

	/**
	 * @return the zdjecie
	 */
	public byte[] getZdjecie()
	{
		return zdjecie;
	}

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }

}
