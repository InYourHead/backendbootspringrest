package com.inyourhead.backend.data.repositories;

import com.inyourhead.backend.data.dto.*;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;
import com.inyourhead.backend.exceptions.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
@Transactional("transactionManager")
public interface DataRepositoryCustom {


    List<SklepDTO> findStoriesByUserId(Integer userId) throws EntityNotFoundException;

    SklepDTO createNewStore(Integer userId, SklepDTO store) throws EntityNotCreatedException, EntityNotFoundException;

    Integer deleteAllStoriesOfSelectedUser(Integer userId) throws EntityNotDeletedException;

    Integer deleteSelectedStoreOfUser(Integer storeId, Integer userId) throws EntityNotDeletedException;

    SklepDTO updateSelectedStore(Integer userId, Integer storeId, SklepDTO store) throws EntityNotFoundException, EntityNotUpdatedException;

    UzytkownikDTO findUserById(Integer userId) throws EntityNotFoundException;

    Integer deleteUserById(Integer userId) throws EntityNotDeletedException;

    UzytkownikDTO updateUser(Integer userId, UzytkownikDTO user) throws EntityNotFoundException;

    List<HistoriaDTO> findUserHistoryByUserId(Integer userId);

    Integer deleteHistoryByUserId(Integer userId) throws EntityNotDeletedException;

    List<ProduktDTO> findProductsByUserIdAndStoreId(Integer userId, Integer storeId) throws EntityNotFoundException;

    Integer deleteProductsByUserIdAndStoreId(Integer userId, Integer storeId) throws EntityNotDeletedException;

    ProduktDTO createNewProduct(Integer userId, Integer storeId, ProduktDTO product) throws EntityNotFoundException, EntityNotCreatedException;

    Integer deleteSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotDeletedException;

    ProduktDTO updateSelectedProduct(Integer userId, Integer storeId, Integer productId, ProduktDTO product) throws EntityNotFoundException, EntityNotUpdatedException;

    PromocjaDTO getSaleForSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotFoundException;

    Integer deleteSaleForSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotDeletedException;

    PromocjaDTO updateSelectedSale(Integer userId, Integer storeId, Integer productId, PromocjaDTO sale) throws EntityNotUpdatedException, EntityNotFoundException;

    PromocjaDTO createNewSale(Integer userId, Integer storeId, Integer productId, PromocjaDTO sale) throws EntityExistsException, EntityNotFoundException, EntityNotCreatedException;

    List<WynikDTO> getSearchResult(ZapytanieDTO fraza, Boolean szukajWOpisie, Integer ilosc) throws EntityNotFoundException, NotValidQueryException;

    HistoriaDTO saveSingleQuestion(Integer userId, ZapytanieDTO question) throws EntityNotFoundException;

    List<KategoriaDTO> getAllCategories();

    List<DefaultProductDTO> getAllDefaultProducts();

    DefaultProductDTO saveOrUpdateDefaultProduct(Integer productId, DefaultProductDTO defaultProductDTO) throws EntityNotFoundException;

    OcenaDTO rateSelectedProduct(Integer userId, Integer rate, Integer productId) throws EntityNotUpdatedException, EntityNotFoundException;

    OcenaDTO getSelectedProductRate(Integer userId, Integer productId);

    void changePasswordOfTheSelectedUser(Integer userId, String encryptedPassword) throws EntityNotFoundException, EntityNotUpdatedException;

    DefaultProductDTO createDefaultProduct(DefaultProductDTO newDefaultProduct) throws EntityNotFoundException;

    List<WynikProduktObiektDTOImpl> getProductsOfSelectedStore(Integer storeId, Integer size);
}


