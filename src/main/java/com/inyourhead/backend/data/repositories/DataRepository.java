/**
 *
 */
package com.inyourhead.backend.data.repositories;

import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.entities.Uzytkownik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author aleksander
 */
@Transactional("transactionManager")
public interface DataRepository extends JpaRepository<Uzytkownik, Integer>, DataRepositoryCustom {


    @Query("select u.idUser from Uzytkownik u where u.login =?1 ")
    Integer findUserIdByLogin(String login);

    @Query("SELECT u FROM Uzytkownik u Where u.login=?1 OR u.email=?1")
    Uzytkownik findUserByLoginOrEmail(String login);

    @Query("select u.typ from Uzytkownik u where u.login =?1 ")
    Integer findUserRoleByUserLogin(String name);

    @Query(value = "select new com.inyourhead.backend.data.dto.SklepDTO(s.adresKod, s.adresMiasto, s.adresNrDomu, s.adresUlica, s.adresWoj, s.idSklep, s.nazwa, s.zdjecie, s.facebook, s.www, s.wspolX, s.wspolY, kat.idKat, kat.nazwa) from Sklep s, Kategoria kat, KategoriaSklep katSklep where s.idSklep= :storeId and s.uzytkownikIdUser.idUser= :userId and kat.idKat= katSklep.idKat and s.idSklep = katSklep.idSklep")
    SklepDTO findStoreByUserIdAndStoreId(@Param("userId") Integer userId, @Param("storeId") Integer storeId);

    @Query(value = " SELECT new com.inyourhead.backend.data.dto.ProduktDTO(pSzczeg.idProdSzczeg, p.idProd, pSzczeg.sklepIdSklep.idSklep, k.nazwa, pSzczeg.nazwa, pSzczeg.cena, pSzczeg.opis, pSzczeg.zdjecie) FROM Produkt p, ProduktSzczegoly pSzczeg, Kategoria k where pSzczeg.idProdSzczeg= :produktId AND pSzczeg.sklepIdSklep.idSklep= :storeId AND pSzczeg.sklepIdSklep.uzytkownikIdUser.idUser= :userId AND p.kategoriaIdKat.idKat = k.idKat AND p.idProd = pSzczeg.idProd.idProd")
    ProduktDTO getSelectedProduct(@Param("userId") Integer userId, @Param("storeId") Integer storeId, @Param("produktId") Integer produktId);

}
