package com.inyourhead.backend.data.repositories.token;

import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.entities.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by aleksander on 03.01.17.
 */
@Transactional("h2TokenTransactionManager")
public interface TokenRepository extends JpaRepository<Token, String> {

    Token findByToken(String token);

    @Query("SELECT new com.inyourhead.backend.data.dto.TokenDTO(t.token, t.deviceId) FROM Token t WHERE t.userId= :userId")
    List<TokenDTO> findByUserId(@Param("userId") Integer userId);

    Long deleteByUserId(Integer userId);

    Long deleteByToken(String token);
}
