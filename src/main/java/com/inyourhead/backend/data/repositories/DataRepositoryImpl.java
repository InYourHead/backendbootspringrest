package com.inyourhead.backend.data.repositories;

import com.inyourhead.backend.data.dto.*;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;
import com.inyourhead.backend.data.entities.*;
import com.inyourhead.backend.data.mappingEntities.ProduktDTOMapper;
import com.inyourhead.backend.data.mappingEntities.SklepDTOMapper;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.utils.ImageBase64Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by aleksander on 04.12.16.
 */
@Transactional("transactionManager")
public class DataRepositoryImpl implements DataRepositoryCustom {


    @Autowired
    @Qualifier("entityManagerFactory")
    private EntityManager manager;

    @Override
    public List<SklepDTO> findStoriesByUserId(Integer userId) throws EntityNotFoundException {

        Kategoria tempCategory;
        Query query = manager.createQuery("SELECT s from Sklep s where s.uzytkownikIdUser.idUser=?1 ")
                .setParameter(1, userId);
        List<Sklep> tempResult;

        Query getCategoryDetails = manager.createQuery("SELECT k FROM Kategoria k, KategoriaSklep kSklep WHERE k.idKat=kSklep.idKat.idKat AND kSklep.idSklep.idSklep=?1");

        tempResult = query.getResultList();

        List<SklepDTO> result = new ArrayList<>();
        for (Sklep s : tempResult) {
            try {
                tempCategory = (Kategoria) (getCategoryDetails.setParameter(1, s.getIdSklep())).getSingleResult();
            } catch (NoResultException e) {
                throw new EntityNotFoundException("Wybrany kategoria nie istnieje!");
            }
            SklepDTO tempStoreDTO = s.toSklepDTO();

            tempStoreDTO.setId_kat(tempCategory.getIdKat());
            tempStoreDTO.setKat_nazwa(tempCategory.getNazwa());

            result.add(tempStoreDTO);
        }

        return result;
    }

    @Override
    public SklepDTO createNewStore(Integer userId, SklepDTO store) throws EntityNotCreatedException, EntityNotFoundException {

        Sklep tempResult;
        SklepDTO result;
        Uzytkownik user;
        Sklep newStore = store.toSklep();
        Kategoria tempCategory;

        KategoriaSklep categoryStore = new KategoriaSklep();

        newStore.setIdSklep(0);

        Query findQuery = manager.createQuery("SELECT u FROM Uzytkownik u where u.idUser=?1")
                .setParameter(1, userId);
        Query findCategory = manager.createQuery("SELECT k FROM Kategoria k where k.idKat=?1").setParameter(1, store.getId_kat());

        try {
            user = (Uzytkownik) findQuery.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Użytkownik o podanym id nie istnieje!");
        }
        newStore.setUzytkownikIdUser(user);

        tempResult = manager.merge(newStore);

        if (tempResult == null)
            throw new EntityNotCreatedException("Nie można utworzyć sklepu, sprawdź, czy wymagane pola są uzupełnione!");

        try {
            tempCategory = (Kategoria) findCategory.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Kategoria o podanym id nie istnieje!!");
        }
        categoryStore.setIdKat(tempCategory);
        categoryStore.setIdSklep(tempResult);

        categoryStore = manager.merge(categoryStore);

        if (categoryStore == null)
            throw new EntityNotCreatedException("Nie można ustawić kategorii sklepu. Czy sklep o podanym id już istnieje?");

        result = tempResult.toSklepDTO();
        result.setKat_nazwa(tempCategory.getNazwa());
        result.setId_kat(tempCategory.getIdKat());

        return result;

    }

    @Override
    public Integer deleteAllStoriesOfSelectedUser(Integer userId) throws EntityNotDeletedException {
        Query query = manager.createQuery("DELETE FROM Sklep s WHERE s.uzytkownikIdUser.idUser =?1")
                .setParameter(1, userId);
        Integer count = query.executeUpdate();

        if (count == 0)
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");

        return count;
    }

    @Override
    public Integer deleteSelectedStoreOfUser(Integer storeId, Integer userId) throws EntityNotDeletedException {
        Query query = manager.createQuery("DELETE FROM Sklep s where s.idSklep=?1 and s.uzytkownikIdUser.idUser=?2")
                .setParameter(1, storeId).setParameter(2, userId);

        Integer count = query.executeUpdate();

        if (count == 0)
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");

        return count;
    }

    @Override
    public SklepDTO updateSelectedStore(Integer userId, Integer storeId, SklepDTO store) throws EntityNotFoundException, EntityNotUpdatedException {
        Sklep updateStore = store.toSklep();
        Sklep originalStore;
        KategoriaSklep updateCategory;
        KategoriaSklep tempCategory;
        Kategoria categoryResult;
        SklepDTO result;
        Sklep tempResult;

        Query findStoreCategory = manager.createQuery("SELECT k FROM KategoriaSklep k where k.idSklep.idSklep=?1").setParameter(1, storeId);

        Query findQuery = manager.createQuery("select s FROM Sklep s where s.idSklep=?1 and s.uzytkownikIdUser.idUser=?2")
                .setParameter(1, storeId).setParameter(2, userId);

        try {
            originalStore = (Sklep) findQuery.getSingleResult();
            tempCategory = (KategoriaSklep) findStoreCategory.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany sklep, bądź kategoria sklepu nie istnieje!");
        }

        Query findCategoryDetails = manager.createQuery("SELECT k FROM Kategoria k where k.idKat=?1").setParameter(1, tempCategory.getIdKat().getIdKat());


        if (updateStore.getAdresKod() != null) originalStore.setAdresKod(updateStore.getAdresKod());

        if (updateStore.getAdresMiasto() != null) originalStore.setAdresMiasto(updateStore.getAdresMiasto());

        if (updateStore.getAdresNrDomu() != null) originalStore.setAdresNrDomu(updateStore.getAdresNrDomu());

        if (updateStore.getAdresUlica() != null) originalStore.setAdresUlica(updateStore.getAdresUlica());

        if (updateStore.getAdresWoj() != null) originalStore.setAdresWoj(updateStore.getAdresWoj());

        if (updateStore.getNazwa() != null) originalStore.setNazwa(updateStore.getNazwa());

        if (updateStore.getZdjecie() != null) originalStore.setZdjecie(updateStore.getZdjecie());

        if (updateStore.getFacebook() != null) originalStore.setFacebook(updateStore.getFacebook());

        if (updateStore.getWww() != null) originalStore.setWww(updateStore.getWww());

        if (updateStore.getWspolX() != null) originalStore.setWspolX(updateStore.getWspolX());

        if (updateStore.getWspolY() != null) originalStore.setWspolY(updateStore.getWspolY());

        try {
            categoryResult = (Kategoria) findCategoryDetails.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrana kategoria sklepu nie istnieje!");
        }

        if (store.getId_kat() != null) {
            tempCategory.setIdKat(categoryResult);

            updateCategory = manager.merge(tempCategory);

            if (updateCategory == null)
                throw new EntityNotUpdatedException("Nie można zaktualizować kategorii sklepu");
        }
        tempResult = manager.merge(originalStore);

        if (tempResult == null) throw new EntityNotUpdatedException("Nie można zaktualizować sklepu.");


        result = tempResult.toSklepDTO();

        result.setId_kat(categoryResult.getIdKat());
        result.setKat_nazwa(categoryResult.getNazwa());

        return result;

    }

    @Override
    public UzytkownikDTO findUserById(Integer userId) throws EntityNotFoundException {
        UzytkownikDTO user;


        Query findQuery = manager.createQuery("SELECT u FROM Uzytkownik u WHERE u.idUser=?1")
                .setParameter(1, userId);
        try {
            user = new UzytkownikDTO((Uzytkownik) findQuery.getSingleResult());
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Użytkownik z podanym id nie istnieje!");
        }

        user.setHaslo(null);

        return user;
    }

    @Override
    public Integer deleteUserById(Integer userId) throws EntityNotDeletedException {
        Query query = manager.createQuery("DELETE FROM Uzytkownik u where u.idUser=?1")
                .setParameter(1, userId);

        Integer count = query.executeUpdate();

        if (count == 0)
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");

        return count;
    }

    @Override
    public UzytkownikDTO updateUser(Integer userId, UzytkownikDTO user) throws EntityNotFoundException {

        Uzytkownik tempResult;
        UzytkownikDTO result;
        Uzytkownik updateUser = user.toUzytkownik();
        Uzytkownik originalUser;


        Query findQuery = manager.createQuery("select u FROM Uzytkownik u where u.idUser=?1")
                .setParameter(1, userId);
        try {
            originalUser = (Uzytkownik) findQuery.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany użytkownik nie istnieje!");
        }

        if (updateUser.getAdresKod() != null) originalUser.setAdresKod(updateUser.getAdresKod());

        if (updateUser.getAdresMiasto() != null) originalUser.setAdresMiasto(updateUser.getAdresMiasto());

        if (updateUser.getAdresNrDomu() != null) originalUser.setAdresNrDomu(updateUser.getAdresNrDomu());

        if (updateUser.getAdresUlica() != null) originalUser.setAdresUlica(updateUser.getAdresUlica());

        if (updateUser.getAdresWoj() != null) originalUser.setAdresWoj(updateUser.getAdresWoj());

        if (updateUser.getEmail() != null) originalUser.setEmail(updateUser.getEmail());

        if (updateUser.getImie() != null) originalUser.setImie(updateUser.getImie());

        if (updateUser.getNazwisko() != null) originalUser.setNazwisko(updateUser.getNazwisko());

        if (updateUser.getTelefon() != null) originalUser.setTelefon(updateUser.getTelefon());


        tempResult = manager.merge(originalUser);

        result= tempResult.toUzytkownikDTO();

        result.setHaslo(null);

        return result;
    }

    @Override
    public void changePasswordOfTheSelectedUser(Integer userId, String encryptedPassword) throws EntityNotFoundException, EntityNotUpdatedException {
        Uzytkownik tempResult;
        Uzytkownik originalUser;

        Query findQuery = manager.createQuery("select u FROM Uzytkownik u where u.idUser=?1")
                .setParameter(1, userId);
        try {
            originalUser = (Uzytkownik) findQuery.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany użytkownik nie istnieje!");
        }


        originalUser.setHaslo(encryptedPassword);

        tempResult = manager.merge(originalUser);

        if (tempResult == null) throw new EntityNotUpdatedException("Nie mozna zmienić hasła!");

    }


    @Override
    public List<HistoriaDTO> findUserHistoryByUserId(Integer userId) {
        Query query = manager.createQuery("SELECT h FROM Historia h where h.uzytkownikIdUser.idUser =?1")
                .setParameter(1, userId);

        List<Historia> tempResult;

        tempResult = query.getResultList();


        List<HistoriaDTO> result = new ArrayList<>();

        for (Historia h : tempResult) {
            result.add(h.toHistoriaDTO());
        }

        return result;
    }

    @Override
    public Integer deleteHistoryByUserId(Integer userId) throws EntityNotDeletedException {

        Query query = manager.createQuery("DELETE FROM Historia h where h.uzytkownikIdUser.idUser=?1")
                .setParameter(1, userId);

        Integer count = query.executeUpdate();

        if (count == 0)
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");

        return count;

    }

    @Override
    public List<ProduktDTO> findProductsByUserIdAndStoreId(Integer userId, Integer storeId) throws EntityNotFoundException {

        Query productDetailsQuery = manager.createQuery("SELECT pr FROM ProduktSzczegoly pr WHERE pr.sklepIdSklep.idSklep = ?1 AND pr.sklepIdSklep.uzytkownikIdUser.idUser=?2")
                .setParameter(1, storeId).setParameter(2, userId);

        List<ProduktSzczegoly> produktSzczegolyList;


        produktSzczegolyList = productDetailsQuery.getResultList();

        List<ProduktDTO> result = new ArrayList<>();

        for (ProduktSzczegoly p : produktSzczegolyList) {
            ProduktDTO tempProduct = new ProduktDTO(p.getIdProdSzczeg(), p.getIdProd().getIdProd(), p.getSklepIdSklep().getIdSklep(), p.getIdProd().getKategoriaIdKat().getNazwa(), p.getNazwa(), p.getCena(), p.getOpis(), ImageBase64Converter.encodeToString(p.getZdjecie()));
            result.add(tempProduct);
        }

        return result;
    }

    @Override
    public Integer deleteProductsByUserIdAndStoreId(Integer userId, Integer storeId) throws EntityNotDeletedException {
        Query query = manager.createQuery("SELECT pr FROM ProduktSzczegoly pr WHERE pr.sklepIdSklep.idSklep = ?1 AND pr.sklepIdSklep.uzytkownikIdUser.idUser=?2")
                .setParameter(1, storeId).setParameter(2, userId);

        List<ProduktSzczegoly> result = query.getResultList();

        Integer count = 0;

        for (ProduktSzczegoly p : result) {
            manager.remove(p);
            count++;
        }

        return count;
    }

    @Override
    public ProduktDTO createNewProduct(Integer userId, Integer storeId, ProduktDTO product) throws EntityNotFoundException, EntityNotCreatedException {

        ProduktSzczegoly tempResult;

        ProduktSzczegoly newProduct = new ProduktSzczegoly();

        Sklep productStore;

        Produkt defaultProduct;


        Query findProduct = manager.createQuery("SELECT p FROM Produkt p WHERE p.idProd=?1")
                .setParameter(1, product.getId_prod());
        Query findStore = manager.createQuery("SELECT s FROM Sklep s WHERE s.uzytkownikIdUser.idUser=?1 AND s.idSklep=?2")
                .setParameter(1, userId).setParameter(2, storeId);


        try {
            defaultProduct = (Produkt) findProduct.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany domyślny produkt nie istnieje!");
        }

        try {
            productStore = (Sklep) findStore.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany sklep nie istnieje!");
        }

        newProduct.setIdProdSzczeg(0);
        newProduct.setIdProd(defaultProduct);
        newProduct.setSklepIdSklep(productStore);
        newProduct.setCena(product.getCena());
        newProduct.setNazwa(product.getNazwa());
        newProduct.setOpis(product.getOpis());
        newProduct.setZdjecie(ImageBase64Converter.decodeToByteArray(product.getZdjecie()));

        tempResult = manager.merge(newProduct);

        if (tempResult == null)
            throw new EntityNotCreatedException("Nie udało się stworzyć produktu! Sprawdź, czy wszystkie wymagane pola są uzupełnione!");

        ProduktDTO result = new ProduktDTO(tempResult.getIdProdSzczeg(), tempResult.getIdProd().getIdProd(), tempResult.getSklepIdSklep().getIdSklep(), tempResult.getIdProd().getKategoriaIdKat().getNazwa(), tempResult.getNazwa(), tempResult.getCena(), tempResult.getOpis(), ImageBase64Converter.encodeToString(tempResult.getZdjecie()));

        return result;

    }

    @Override
    public Integer deleteSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotDeletedException {
        Query query = manager.createQuery("SELECT pr FROM ProduktSzczegoly pr WHERE pr.sklepIdSklep.idSklep = ?1 AND pr.sklepIdSklep.uzytkownikIdUser.idUser=?2 AND pr.idProdSzczeg=?3")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);

        ProduktSzczegoly produkt;

        try {
            produkt = (ProduktSzczegoly) query.getSingleResult();
            manager.remove(produkt);
        } catch (NoResultException ex) {
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");
        }


        return 1;
    }

    @Override
    public ProduktDTO updateSelectedProduct(Integer userId, Integer storeId, Integer productId, ProduktDTO product) throws EntityNotFoundException, EntityNotUpdatedException {
        ProduktSzczegoly tempResult;

        ProduktDTO result;

        Produkt selectedNewProduct;

        ProduktSzczegoly updateProduct = new ProduktSzczegoly(null, null, product.getNazwa(), product.getCena(), product.getOpis(), ImageBase64Converter.decodeToByteArray(product.getZdjecie()));
        updateProduct.setIdProdSzczeg(productId);

        ProduktSzczegoly originalProduct;

        Kategoria category;

        Query findSelectedProductDetails = manager.createQuery("SELECT p FROM ProduktSzczegoly p WHERE p.sklepIdSklep.idSklep = ?1 AND p.sklepIdSklep.uzytkownikIdUser.idUser=?2 AND p.idProdSzczeg= ?3")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);

        if (product.getId_prod() != null) {
            Query findNewDefaultProduct = manager.createQuery("SELECT p FROM Produkt p WHERE p.idProd=?1").setParameter(1, product.getId_prod());

            try {
                selectedNewProduct = (Produkt) findNewDefaultProduct.getSingleResult();
            } catch (NoResultException e) {
                throw new EntityNotFoundException("Wybrany nowy domyslny produkt nie istnieje!");
            }

            updateProduct.setIdProd(selectedNewProduct);
        }

        try {
            originalProduct = (ProduktSzczegoly) findSelectedProductDetails.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany produkt nie istnieje!");
        }

        if (updateProduct.getCena() != null) originalProduct.setCena(updateProduct.getCena());

        if (updateProduct.getNazwa() != null) originalProduct.setNazwa(updateProduct.getNazwa());

        if (updateProduct.getOpis() != null) originalProduct.setOpis(updateProduct.getOpis());

        if (updateProduct.getZdjecie() != null) originalProduct.setZdjecie(updateProduct.getZdjecie());

        if (updateProduct.getIdProd() != null) originalProduct.setIdProd(updateProduct.getIdProd());


        tempResult = manager.merge(originalProduct);

        if (tempResult == null) throw new EntityNotUpdatedException("Nie udało się aktualizować wybranego produktu!");

        result = new ProduktDTO(tempResult.getIdProdSzczeg(), tempResult.getIdProd().getIdProd(), tempResult.getSklepIdSklep().getIdSklep(), tempResult.getIdProd().getNazwa(), tempResult.getNazwa(), tempResult.getCena(), tempResult.getOpis(), ImageBase64Converter.encodeToString(tempResult.getZdjecie()));

        return result;
    }

    @Override
    public PromocjaDTO getSaleForSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotFoundException {
        Promocja result;

        Query findQuery = manager.createQuery("SELECT pr FROM Promocja pr WHERE pr.idProd.idProdSzczeg =?1 AND pr.idSklep.idSklep =?2")
                .setParameter(1, productId).setParameter(2, storeId);

        try {
            result = (Promocja) findQuery.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Brak promocji!");
        }

        return result.toPromocjaDTO();
    }

    @Override
    public Integer deleteSaleForSelectedProduct(Integer userId, Integer storeId, Integer productId) throws EntityNotDeletedException {
        Integer count;

        Query query = manager.createQuery("DELETE FROM Promocja pr WHERE pr.idProm IN(SELECT p.idProm FROM Promocja p WHERE p.idSklep.idSklep = ?1 AND p.idSklep.uzytkownikIdUser.idUser=?2 AND pr.idProd.idProdSzczeg=?3)")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);

        count = query.executeUpdate();

        if (count == 0)
            throw new EntityNotDeletedException("Bład przy usuwaniu. Sprawdź, czy wybrany obiekt istnieje!");

        return count;
    }

    @Override
    public PromocjaDTO updateSelectedSale(Integer userId, Integer storeId, Integer productId, PromocjaDTO sale) throws EntityNotUpdatedException, EntityNotFoundException {
        Promocja result;

        Promocja originalSale;

        Promocja updateSale = sale.toPromocja();

        Query query = manager.createQuery("SELECT pr FROM Promocja pr WHERE  pr.idSklep.idSklep = ?1 AND pr.idSklep.uzytkownikIdUser.idUser=?2 AND pr.idProd.idProdSzczeg=?3")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);
        try {
            originalSale = (Promocja) query.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrana promocja nie istnieje!");
        }
        if (updateSale.getCenaPromocyjna() != null) originalSale.setCenaPromocyjna(updateSale.getCenaPromocyjna());

        result = manager.merge(originalSale);

        if (result == null)
            throw new EntityNotUpdatedException("Nie udało się aktualizować promocji! Czy wybrana promocja istnieje?");

        return result.toPromocjaDTO();
    }

    @Override
    public PromocjaDTO createNewSale(Integer userId, Integer storeId, Integer productId, PromocjaDTO sale) throws EntityExistsException, EntityNotFoundException, EntityNotCreatedException {
        Promocja result;

        Promocja originalSale = null;

        Promocja newSale = sale.toPromocja();

        ProduktSzczegoly findProduct;

        Query query = manager.createQuery("SELECT pr FROM Promocja pr WHERE pr.idSklep.idSklep = ?1 AND pr.idSklep.uzytkownikIdUser.idUser=?2 AND pr.idProd.idProdSzczeg=?3")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);
        Query findProductQuery = manager.createQuery("SELECT p FROM ProduktSzczegoly p WHERE p.sklepIdSklep.idSklep = ?1 AND p.sklepIdSklep.uzytkownikIdUser.idUser=?2 AND p.idProdSzczeg=?3")
                .setParameter(1, storeId).setParameter(2, userId).setParameter(3, productId);

        try {
            originalSale = (Promocja) query.getSingleResult();
        } catch (NoResultException e) {
            //do nothing
        }
        if (originalSale != null) throw new EntityExistsException("Istnieje już promocja przypisana do produktu!");


        try {
            findProduct = (ProduktSzczegoly) findProductQuery.getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Wybrany produkt nie istnieje!");
        }

        newSale.setCenaPromocyjna(sale.getCena_promocyjna());
        newSale.setIdProd(findProduct);
        newSale.setIdSklep(findProduct.getSklepIdSklep());


        result = manager.merge(newSale);

        if (result == null)
            throw new EntityNotCreatedException("Nie udało się utworzyć promocji! Sprawdź, czy wszystkie wymagane pola są wypełnione!");

        return result.toPromocjaDTO();
    }

    @Override
    public List<WynikDTO> getSearchResult(ZapytanieDTO fraza, Boolean szukajWOpisie, Integer ilosc) throws EntityNotFoundException, NotValidQueryException {

        String productQuery = "SELECT null as id_kat,k.nazwa as kat_nazwa, p.id_prod_szczeg as id_prod,prom.cena as cena_promocyjna, AVG(o.ocena) as ocena,IFNULL(p.nazwa,pr.nazwa) AS nazwa,IFNULL(p.opis,pr.opis) AS opis,IFNULL(p.cena,0) as cena,IFNULL(p.zdjecie,pr.zdjecie) as zdjecie, s.facebook,s.www,s.nazwa as nazwa_sklep, s.id_sklep, s.adres_ulica,s.adres_nr_domu,s.adres_kod,s.adres_miasto,s.adres_woj, s.zdjecie as zdjecie_sklep, s.wspol_x as X, s.wspol_y as Y\n" +
                "FROM sklep s\n" +
                "LEFT JOIN prod_szczeg p ON s.id_sklep=p.id_sklep\n" +
                "LEFT JOIN promocja prom ON p.id_prod_szczeg=prom.id_prod\n" +
                "LEFT JOIN produkt pr ON p.id_prod=pr.id_prod\n" +
                "LEFT JOIN kategoria k ON pr.id_kat=k.id_kat\n" +
                "LEFT JOIN oceny_prod o ON p.id_prod_szczeg=o.id_prod_szczeg\n" +
                "WHERE (LOWER(k.nazwa) LIKE LOWER(:fraza) OR LOWER(p.nazwa) LIKE LOWER(:fraza) OR LOWER (p.opis) LIKE LOWER(:frazaOpis) OR LOWER(pr.nazwa) LIKE LOWER(:fraza) OR LOWER (pr.opis) LIKE LOWER(:frazaOpis)) AND ( prom.cena IS NULL OR ((:cenaMin IS NULL OR (prom.cena >= :cenaMin)) AND (:cenaMax IS NULL OR (prom.cena <= :cenaMax)))) AND ( prom.cena IS NOT NULL OR ((:cenaMin IS NULL OR (p.cena >= :cenaMin)) AND (:cenaMax IS NULL OR (p.cena <= :cenaMax)))) AND pr.id_kat NOT IN (:katUsl, :katSkl) GROUP BY p.id_prod_szczeg, prom.cena\n" +
                "ORDER BY ABS(POWER(:lng - s.wspol_x,2) + POWER (:lat - s.wspol_y,2)) LIMIT :ilosc ";

        //.setParameter("lng", fraza.getLng()).setParameter("lat", fraza.getLAt()).setParameter("ilosc", ilosc);
        //" ORDER BY ABS(POWER(:lng - x,2) + POWER(:lat - y,2)) LIMIT :ilosc "
        String productWithSelectedCategoryQuery = "SELECT :idKat as id_kat ,k.nazwa as kat_nazwa,p.id_prod_szczeg as id_prod, prom.cena as cena_promocyjna, AVG(o.ocena) as ocena,IFNULL(p.nazwa,pr.nazwa) AS nazwa,IFNULL(p.opis,pr.opis) AS opis,IFNULL(p.cena,0) as cena,IFNULL(p.zdjecie,pr.zdjecie) as zdjecie,s.id_sklep, s.facebook,s.www,s.nazwa as nazwa_sklep,s.adres_ulica,s.adres_nr_domu,s.adres_kod,s.adres_miasto,s.adres_woj, s.zdjecie as zdjecie_sklep, s.wspol_x as X, s.wspol_y as Y\n" +
                "FROM sklep s\n" +
                "LEFT JOIN prod_szczeg p ON s.id_sklep=p.id_sklep\n" +
                "LEFT JOIN promocja prom ON p.id_prod_szczeg=prom.id_prod\n" +
                "LEFT JOIN produkt pr ON p.id_prod=pr.id_prod\n" +
                "LEFT JOIN kategoria k ON pr.id_kat=k.id_kat\n" +
                "LEFT JOIN oceny_prod o ON p.id_prod_szczeg=o.id_prod_szczeg\n" +
                "WHERE (LOWER(k.nazwa) LIKE LOWER(:fraza) OR LOWER(p.nazwa) LIKE LOWER(:fraza) OR LOWER (p.opis) LIKE LOWER(:frazaOpis) OR LOWER(pr.nazwa) LIKE LOWER(:fraza) OR LOWER (pr.opis) LIKE LOWER(:frazaOpis)) AND ( prom.cena IS NULL OR ((:cenaMin IS NULL OR (prom.cena >= :cenaMin)) AND (:cenaMax IS NULL OR (prom.cena <= :cenaMax)))) AND ( prom.cena IS NOT NULL OR ((:cenaMin IS NULL OR (p.cena >= :cenaMin)) AND (:cenaMax IS NULL OR (p.cena <= :cenaMax)))) AND pr.id_kat= :idKat GROUP BY p.id_prod_szczeg, prom.cena\n" +
                "ORDER BY ABS(POWER(:lng - s.wspol_x,2) + POWER (:lat - s.wspol_y,2)) LIMIT :ilosc ";

        String storeQuery = "SELECT s.id_sklep, s.facebook, s.www, s.nazwa as nazwa, s.adres_ulica, s.adres_nr_domu, s.adres_kod, s.adres_miasto, s.adres_woj, s.zdjecie, s.wspol_x as X, s.wspol_y as Y, k.nazwa as kat_nazwa FROM sklep s, kategoria k, kat_sklep ks WHERE ks.id_sklep=s.id_sklep AND ks.id_kat=k.id_kat AND (LOWER(s.nazwa) LIKE LOWER(:fraza) OR LOWER(k.nazwa) LIKE LOWER(:fraza))\n" +
                "ORDER BY ABS(POWER(:lng - s.wspol_x,2) + POWER (:lat - s.wspol_y,2)) LIMIT :ilosc ";


        Query getResultListOfProductsWithDefaultCategory = manager.createNativeQuery(productQuery, ProduktDTOMapper.class);
        Query getResultListOfProductsWithSelectedCategory = manager.createNativeQuery(productWithSelectedCategoryQuery, ProduktDTOMapper.class);
        Query getResultListOfStores = manager.createNativeQuery(storeQuery, SklepDTOMapper.class);

        List<WynikDTO> result = new ArrayList<>();

        Integer katUsl = 2, katSkl = 1;

        String frazaOpis = null;

        //wyszukiwanie w opisie
        if (szukajWOpisie)
            frazaOpis = "%" + fraza.getFraza() + "%";

        if (fraza.getId_kat() == null) {  //szukamy w produktach

            getResultListOfProductsWithDefaultCategory
                    .setParameter("fraza", "%" + fraza.getFraza() + "%").setParameter("frazaOpis", "%" + frazaOpis + "%")
                    .setParameter("cenaMin", fraza.getCena_min()).setParameter("cenaMax", fraza.getCena_max())
                    .setParameter("katUsl", katUsl).setParameter("katSkl", katSkl)
                    .setParameter("lng", fraza.getLng()).setParameter("lat", fraza.getLat()).setParameter("ilosc", ilosc);

            List<ProduktDTOMapper> tempResult;


            tempResult = (List<ProduktDTOMapper>) getResultListOfProductsWithDefaultCategory.getResultList();


            for (Object w : tempResult) {
                WynikProduktObiektDTOImpl temp = ((ProduktDTOMapper) w).toWynikProduktObiektDTOImpl();

                temp.getSklep().setId_kat(1);
                result.add(temp);
            }

        } else {
            try {
                manager.createQuery("SELECT k FROM Kategoria k WHERE k.idKat =?1")
                        .setParameter(1, fraza.getId_kat()).getSingleResult();
            } catch (NoResultException e) {
                throw new EntityNotFoundException("Wybrana kategoria nie istnieje!");
            }


            if (fraza.getId_kat().equals(katSkl)) { //szukamy w sklepach

                List<SklepDTOMapper> tempResult;
                getResultListOfStores.setParameter("fraza", "%" + fraza.getFraza() + "%")
                        .setParameter("lng", fraza.getLng()).setParameter("lat", fraza.getLat()).setParameter("ilosc", ilosc);

                tempResult = (List<SklepDTOMapper>) getResultListOfStores.getResultList();

                for (SklepDTOMapper s : tempResult) {
                    result.add(s.toWynikSklepDTOImpl());
                }

            } else { //wyszukujemy z wybraniem kategorii

                getResultListOfProductsWithSelectedCategory.setParameter("fraza", "%" + fraza.getFraza() + "%")
                        .setParameter("frazaOpis", "%" + frazaOpis + "%").setParameter("cenaMin", fraza.getCena_min())
                        .setParameter("cenaMax", fraza.getCena_max()).setParameter("idKat", fraza.getId_kat())
                        .setParameter("lng", fraza.getLng()).setParameter("lat", fraza.getLat()).setParameter("ilosc", ilosc);

                List<ProduktDTOMapper> tempResult = (List<ProduktDTOMapper>) getResultListOfProductsWithSelectedCategory.getResultList();

                for (Object w : tempResult) {
                    result.add(((ProduktDTOMapper) w).toWynikProduktObiektDTOImpl());
                }

            }
        }
        return result;
    }

    @Override
    public List<WynikProduktObiektDTOImpl> getProductsOfSelectedStore(Integer storeId, Integer size) {

        List<WynikProduktObiektDTOImpl> result = new ArrayList<>();

        List<ProduktDTOMapper> tempResult;


        String productQuery = "SELECT k.id_kat,k.nazwa as kat_nazwa, p.id_prod_szczeg as id_prod,prom.cena as cena_promocyjna, AVG(o.ocena) as ocena,IFNULL(p.nazwa,pr.nazwa) AS nazwa,IFNULL(p.opis,pr.opis) AS opis,IFNULL(p.cena,0) as cena,IFNULL(p.zdjecie,pr.zdjecie) as zdjecie, s.facebook,s.www,s.nazwa as nazwa_sklep, s.id_sklep, s.adres_ulica,s.adres_nr_domu,s.adres_kod,s.adres_miasto,s.adres_woj, s.zdjecie as zdjecie_sklep, s.wspol_x as X, s.wspol_y as Y\n" +
                "FROM sklep s\n" +
                "LEFT JOIN prod_szczeg p ON s.id_sklep=p.id_sklep\n" +
                "LEFT JOIN promocja prom ON p.id_prod_szczeg=prom.id_prod\n" +
                "LEFT JOIN produkt pr ON p.id_prod=pr.id_prod\n" +
                "LEFT JOIN kategoria k ON pr.id_kat=k.id_kat\n" +
                "LEFT JOIN oceny_prod o ON p.id_prod_szczeg=o.id_prod_szczeg\n" +
                "WHERE s.id_sklep=?1 GROUP BY p.id_prod_szczeg, prom.cena ORDER BY nazwa LIMIT :size";


        Query findStoreProducts = manager.createNativeQuery(productQuery, ProduktDTOMapper.class).setParameter(1, storeId).setParameter("size", size);

        tempResult = (List<ProduktDTOMapper>) findStoreProducts.getResultList();

        for (Object w : tempResult) {

            if (w != null) {

                WynikProduktObiektDTOImpl temp = ((ProduktDTOMapper) w).toWynikProduktObiektDTOImpl();
                temp.setSklep(null);
                result.add(temp);
            }
        }
        return result;
    }

    @Override
    public HistoriaDTO saveSingleQuestion(Integer userId, ZapytanieDTO question) throws EntityNotFoundException {
        Boolean questionExists = true;
        Historia history;
        Historia tempResult = null;
        HistoriaDTO result = null;
        Uzytkownik user;
        Query query = manager.createQuery("SELECT h FROM Historia h where h.uzytkownikIdUser.idUser =?1 AND LOWER(h.fraza) =LOWER(?2) AND (h.cenaMin = ?3 OR (h.cenaMin IS NULL AND ?3 IS NULL)) AND (h.cenaMax=?4 OR (h.cenaMax IS NULL AND ?4 IS NULL)) AND (h.idKat=?5 OR (h.idKat IS NULL AND ?5 IS NULL))")
                .setParameter(1, userId).setParameter(2, question.getFraza()).setParameter(3, question.getCena_min()).setParameter(4, question.getCena_max()).setParameter(5, question.getId_kat());


        try {
            tempResult = ((Historia) query.getSingleResult());
        } catch (NoResultException e) {
            questionExists = false;
        }

        if (!questionExists) {

            Query findQuery = manager.createQuery("SELECT u FROM Uzytkownik u WHERE u.idUser=?1")
                    .setParameter(1, userId);

            try {
                user = (Uzytkownik) findQuery.getSingleResult();
            } catch (NoResultException e) {
                throw new EntityNotFoundException("Użytkownik z podanym id nie istnieje!");
            }
            //stworzmy historię!

            history = new Historia(question.getFraza(), question.getId_kat(), question.getCena_min(), question.getCena_max());
            history.setNr(0);
            history.setUzytkownikIdUser(user);

            tempResult = manager.merge(history);

        } else {
            Date newDate = new Date();
            tempResult.setData(newDate);

            manager.merge(tempResult);

        }
        return tempResult.toHistoriaDTO();
    }

    @Override
    public List<KategoriaDTO> getAllCategories() {
        List<Kategoria> tempResult;
        List<KategoriaDTO> result = new ArrayList<>();

        Query findCategories = manager.createQuery("SELECT k FROM Kategoria k");

        tempResult = findCategories.getResultList();

        for (Kategoria k : tempResult) {
            result.add(k.toKategoriaDTO());
        }

        return result;
    }

    @Override
    public List<DefaultProductDTO> getAllDefaultProducts() {
        List<DefaultProductDTO> result = new ArrayList<>();
        List<Produkt> tempResult;

        Query findDefaultProducts = manager.createQuery("SELECT p FROM Produkt p ORDER BY p.nazwa");

        tempResult = findDefaultProducts.getResultList();

        for (Produkt p : tempResult) {

            result.add(new DefaultProductDTO(p.getIdProd(), p.getNazwa(), p.getOpis(), ImageBase64Converter.encodeToString(p.getZdjecie()), p.getKategoriaIdKat().getIdKat()));

        }

        return result;
    }

    @Override
    public DefaultProductDTO createDefaultProduct(DefaultProductDTO newDefaultProduct) throws EntityNotFoundException {
        DefaultProductDTO response;

        Kategoria defaultProductCategory = null;

        Produkt tempResponse = null;
        Produkt newEntity = new Produkt();

        Query findCategoryById = manager.createQuery("SELECT k FROM Kategoria k WHERE k.idKat=?1").setParameter(1, newDefaultProduct.getId_kat());

        try {
            defaultProductCategory = (Kategoria) findCategoryById.getSingleResult();
        } catch (NoResultException ex) {
            throw new EntityNotFoundException("Kategoria o podanym id nie istnieje!");
        }

        newEntity.setIdProd(0);
        newEntity.setZdjecie(ImageBase64Converter.decodeToByteArray(newDefaultProduct.getZdjecie()));
        newEntity.setOpis(newDefaultProduct.getOpis());
        newEntity.setNazwa(newDefaultProduct.getNazwa());
        newEntity.setKategoriaIdKat(defaultProductCategory);

        tempResponse = manager.merge(newEntity);

        response = new DefaultProductDTO(tempResponse.getIdProd(), tempResponse.getNazwa(), tempResponse.getOpis(), ImageBase64Converter.encodeToString(tempResponse.getZdjecie()), tempResponse.getKategoriaIdKat().getIdKat());

        return response;
    }


    @Override
    public DefaultProductDTO saveOrUpdateDefaultProduct(Integer productId, DefaultProductDTO defaultProductDTO) throws EntityNotFoundException {

        Produkt originalProduct;
        Produkt tempResult;
        DefaultProductDTO result;
        Kategoria kategoria;

        if (productId != null) {
            try {
                originalProduct = (Produkt) manager.createQuery("SELECT p FROM Produkt p WHERE p.idProd=?1").setParameter(1, productId).getSingleResult();
            } catch (NoResultException e) {
                throw new EntityNotFoundException("Produkt o podanym id nie istnieje!");
            }
        } else {
            originalProduct = new Produkt();
            originalProduct.setIdProd(0);
            try {
                kategoria = (Kategoria) manager.createQuery("SELECT k FROM Kategoria k WHERE k.idKat=?1").setParameter(1, defaultProductDTO.getId_kat()).getSingleResult();
            } catch (NoResultException ex) {
                throw new EntityNotFoundException("Wybrana kategoria nie istnieje!");
            }
            originalProduct.setKategoriaIdKat(kategoria);
        }

        if (defaultProductDTO.getNazwa() != null) originalProduct.setNazwa(defaultProductDTO.getNazwa());
        if (defaultProductDTO.getOpis() != null) originalProduct.setOpis(defaultProductDTO.getOpis());
        if (defaultProductDTO.getZdjecie() != null)
            originalProduct.setZdjecie(ImageBase64Converter.decodeToByteArray(defaultProductDTO.getZdjecie()));

        tempResult = manager.merge(originalProduct);


        return new DefaultProductDTO(tempResult.getIdProd(), tempResult.getNazwa(), tempResult.getOpis(), ImageBase64Converter.encodeToString(tempResult.getZdjecie()), tempResult.getKategoriaIdKat().getIdKat());

    }

    @Override
    public OcenaDTO rateSelectedProduct(Integer userId, Integer rate, Integer productId) throws EntityNotUpdatedException, EntityNotFoundException {
        OcenyProdukt queryResult = null;

        OcenyProdukt tempResult;

        OcenaDTO result;

        Query findExistingRate = manager.createQuery("SELECT r FROM OcenyProdukt r WHERE r.idUser.idUser=?1 AND r.idProd.idProdSzczeg=?2")
                .setParameter(1, userId).setParameter(2, productId);

        Query findExistingUser = manager.createQuery("SELECT u FROM Uzytkownik u WHERE u.idUser=?1")
                .setParameter(1, userId);

        Query findExistingProduct = manager.createQuery("SELECT p FROM ProduktSzczegoly p WHERE p.idProdSzczeg=?1")
                .setParameter(1, productId);

        try {
            queryResult = (OcenyProdukt) findExistingRate.getSingleResult();
        } catch (NoResultException e) {

            Uzytkownik user;
            ProduktSzczegoly product;

            queryResult = new OcenyProdukt();

            try {
                user = (Uzytkownik) findExistingUser.getSingleResult();
            } catch (NoResultException ex) {
                throw new EntityNotFoundException("Wybrany użytkownik nie istnieje!");
            }
            try {
                product = (ProduktSzczegoly) findExistingProduct.getSingleResult();
            } catch (NoResultException ex) {
                throw new EntityNotFoundException("Wybrany produkt nie istnieje!");
            }
            queryResult.setIdProd(product);
            queryResult.setIdUser(user);

        } finally {
            queryResult.setOcena(rate);
            tempResult = manager.merge(queryResult);
        }

        if (tempResult == null)
            throw new EntityNotUpdatedException("Nie można ustawić oceny produktu! Sprawdż, czy produkt oraz użytkownik istnieje");

        result = new OcenaDTO(tempResult.getOcena());

        return result;
    }

    @Override
    public OcenaDTO getSelectedProductRate(Integer userId, Integer productId) {

        OcenaDTO result;

        OcenyProdukt tempResult = new OcenyProdukt();

        Query findSelectedProductRate = manager.createQuery("SELECT r FROM OcenyProdukt r WHERE r.idUser.idUser=?1 AND r.idProd.idProdSzczeg=?2")
                .setParameter(1, userId).setParameter(2, productId);

        try {
            tempResult = (OcenyProdukt) findSelectedProductRate.getSingleResult();
        } catch (NoResultException e) {
            tempResult = new OcenyProdukt();
            tempResult.setOcena(null);
        } finally {
            result = new OcenaDTO(tempResult.getOcena());
        }

        return result;
    }
}
