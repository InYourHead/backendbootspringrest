package com.inyourhead.backend.data.mappingEntities;

import com.inyourhead.backend.data.dto.interfaces.WynikDTO;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by aleksander on 02.04.17.
 */
@Entity
public class SklepDTOMapper implements WynikDTO {

    @Id
    private Integer id_sklep;
    private String facebook;
    private String www;
    private String nazwa;
    private String adres_ulica;
    private String adres_nr_domu;
    private String adres_kod;
    private String adres_miasto;
    private String adres_woj;
    private byte[] zdjecie;
    private Double X;
    private Double Y;
    private String kat_nazwa;

    public SklepDTOMapper() {
        super();
    }

    public SklepDTOMapper(Integer id_sklep, String facebook, String www, String nazwa, String adres_ulica, String adres_nr_domu, String adres_kod, String adres_miasto, String adres_woj, byte[] zdjecie, Double X, Double Y, String kat_nazwa) {
        this.id_sklep = id_sklep;
        this.facebook = facebook;
        this.www = www;
        this.nazwa = nazwa;
        this.adres_ulica = adres_ulica;
        this.adres_nr_domu = adres_nr_domu;
        this.adres_kod = adres_kod;
        this.adres_miasto = adres_miasto;
        this.adres_woj = adres_woj;
        this.zdjecie = zdjecie;
        this.X = X;
        this.Y = Y;
        this.kat_nazwa = kat_nazwa;
    }

    @Override
    public Double getX() {
        return this.X;
    }

    @Override
    public void setX(Double x) {
        this.X = x;
    }

    @Override
    public Double getY() {
        return this.Y;
    }

    @Override
    public void setY(Double y) {

        this.Y = y;

    }

    public Integer getId_sklep() {
        return id_sklep;
    }

    public void setId_sklep(Integer id_sklep) {
        this.id_sklep = id_sklep;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres_ulica() {
        return adres_ulica;
    }

    public void setAdres_ulica(String adres_ulica) {
        this.adres_ulica = adres_ulica;
    }

    public String getAdres_nr_domu() {
        return adres_nr_domu;
    }

    public void setAdres_nr_domu(String adres_nr_domu) {
        this.adres_nr_domu = adres_nr_domu;
    }

    public String getAdres_kod() {
        return adres_kod;
    }

    public void setAdres_kod(String adres_kod) {
        this.adres_kod = adres_kod;
    }

    public String getAdres_miasto() {
        return adres_miasto;
    }

    public void setAdres_miasto(String adres_miasto) {
        this.adres_miasto = adres_miasto;
    }

    public String getAdres_woj() {
        return adres_woj;
    }

    public void setAdres_woj(String adres_woj) {
        this.adres_woj = adres_woj;
    }

    public byte[] getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public com.inyourhead.backend.data.dto.WynikSklepDTOImpl toWynikSklepDTOImpl() {

        com.inyourhead.backend.data.dto.WynikSklepDTOImpl result = new com.inyourhead.backend.data.dto.WynikSklepDTOImpl(1, this.kat_nazwa, this.id_sklep, this.facebook, this.www, this.nazwa, this.adres_ulica, this.adres_nr_domu, this.adres_kod, this.adres_miasto, this.adres_woj, this.zdjecie, this.X, this.Y);

        return result;
    }
}
