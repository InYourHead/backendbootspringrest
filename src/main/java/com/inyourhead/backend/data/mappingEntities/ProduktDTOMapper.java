package com.inyourhead.backend.data.mappingEntities;

import com.inyourhead.backend.data.dto.WynikProduktObiektDTOImpl;
import com.inyourhead.backend.data.dto.WynikSklepDTOImpl;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by aleksander on 28.11.16.
 */

@Entity
public class ProduktDTOMapper implements WynikDTO {

    //to musi być, żebym mógł sobię mapować wyniki z nativeQuery (ile pierdolenia z tym było, łobożeee
    @Id
    private Integer id_prod;
    private String kat_nazwa;
    private Double ocena;
    private String nazwa;
    private String opis;
    private Double cena;
    private byte[] zdjecie;
    private Integer id_sklep;
    private String facebook;
    private String www;
    private String nazwa_sklep;
    private String adres_ulica;
    private String adres_nr_domu;
    private String adres_kod;
    private String adres_miasto;
    private String adres_woj;
    private byte[] zdjecie_sklep;
    private Double cena_promocyjna;
    private Double X;
    private Double Y;

    public ProduktDTOMapper() {

    }

    public ProduktDTOMapper(Integer id_prod, String kat_nazwa, Double ocena, String nazwa, String opis, Double cena, byte[] zdjecie, Integer id_sklep, String facebook, String www, String nazwa_sklep, String adres_ulica, String adres_nr_domu, String adres_kod, String adres_miasto, String adres_woj, byte[] zdjecie_sklep, Double cena_promocyjna, Double x, Double y) {
        this.id_prod = id_prod;
        this.kat_nazwa = kat_nazwa;
        this.ocena = ocena;
        this.nazwa = nazwa;
        this.opis = opis;
        this.cena = cena;
        this.zdjecie = zdjecie;
        this.id_sklep = id_sklep;
        this.facebook = facebook;
        this.www = www;
        this.nazwa_sklep = nazwa_sklep;
        this.adres_ulica = adres_ulica;
        this.adres_nr_domu = adres_nr_domu;
        this.adres_kod = adres_kod;
        this.adres_miasto = adres_miasto;
        this.adres_woj = adres_woj;
        this.zdjecie_sklep = zdjecie_sklep;
        this.cena_promocyjna = cena_promocyjna;
        X = x;
        Y = y;
    }

    public Integer getId_prod() {
        return id_prod;
    }

    public void setId_prod(Integer id_prod) {
        this.id_prod = id_prod;
    }

    public String getKat_nazwa() {
        return kat_nazwa;
    }

    public void setKat_nazwa(String kat_nazwa) {
        this.kat_nazwa = kat_nazwa;
    }

    public Double getOcena() {
        return ocena;
    }

    public void setOcena(Double ocena) {
        this.ocena = ocena;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public byte[] getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(byte[] zdjecie) {
        this.zdjecie = zdjecie;
    }

    public Integer getId_sklep() {
        return id_sklep;
    }

    public void setId_sklep(Integer id_sklep) {
        this.id_sklep = id_sklep;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getNazwa_sklep() {
        return nazwa_sklep;
    }

    public void setNazwa_sklep(String nazwa_sklep) {
        this.nazwa_sklep = nazwa_sklep;
    }

    public String getAdres_ulica() {
        return adres_ulica;
    }

    public void setAdres_ulica(String adres_ulica) {
        this.adres_ulica = adres_ulica;
    }

    public String getAdres_nr_domu() {
        return adres_nr_domu;
    }

    public void setAdres_nr_domu(String adres_nr_domu) {
        this.adres_nr_domu = adres_nr_domu;
    }

    public String getAdres_kod() {
        return adres_kod;
    }

    public void setAdres_kod(String adres_kod) {
        this.adres_kod = adres_kod;
    }

    public String getAdres_miasto() {
        return adres_miasto;
    }

    public void setAdres_miasto(String adres_miasto) {
        this.adres_miasto = adres_miasto;
    }

    public String getAdres_woj() {
        return adres_woj;
    }

    public void setAdres_woj(String adres_woj) {
        this.adres_woj = adres_woj;
    }

    public byte[] getZdjecie_sklep() {
        return zdjecie_sklep;
    }

    public void setZdjecie_sklep(byte[] zdjecie_sklep) {
        this.zdjecie_sklep = zdjecie_sklep;
    }

    public Double getCena_promocyjna() {
        return cena_promocyjna;
    }

    public void setCena_promocyjna(Double cena_promocyjna) {
        this.cena_promocyjna = cena_promocyjna;
    }


    public WynikProduktObiektDTOImpl toWynikProduktObiektDTOImpl() {

        Double cena_końcowa = this.cena;

        if (this.cena_promocyjna != null) {
            cena_końcowa = this.cena_promocyjna;
        }

        return new WynikProduktObiektDTOImpl(null, this.kat_nazwa, this.id_prod, this.ocena, this.nazwa, this.opis, cena_końcowa, this.zdjecie, new WynikSklepDTOImpl(null, null, this.id_sklep, this.facebook, this.www, this.nazwa_sklep, this.adres_ulica, this.adres_nr_domu, this.adres_kod, this.adres_miasto, this.adres_woj, this.zdjecie_sklep, this.X, this.Y));
    }

    @Override
    public Double getX() {
        return this.X;
    }

    @Override
    public void setX(Double x) {
        this.X = x;
    }

    @Override
    public Double getY() {
        return this.Y;
    }

    @Override
    public void setY(Double y) {

        this.Y = y;

    }
}
