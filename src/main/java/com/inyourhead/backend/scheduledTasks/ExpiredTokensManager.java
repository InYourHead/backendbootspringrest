package com.inyourhead.backend.scheduledTasks;

import com.inyourhead.backend.data.entities.Token;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by aleksander on 13.04.17.
 */
@Component
public class ExpiredTokensManager {

    private TokenRepository tokenRepository;

    @Autowired
    public ExpiredTokensManager(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void deleteExpiredTokens() {
        List<Token> tokens = tokenRepository.findAll();

        Date currentDate = new Date();

        for (Token t : tokens) {

            if (t.getDeviceId().equals("RESET_PASSWORD")) {
                if (add24HoursToDate(t.getCreateDate()).before(currentDate)) {
                    tokenRepository.delete(t);
                }
            }
        }
    }

    private Date add24HoursToDate(Date date) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        return c.getTime();

    }
}
