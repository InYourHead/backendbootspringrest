package com.inyourhead.backend.scheduledTasks;

import com.inyourhead.backend.data.dto.DefaultProductDTO;
import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.EntityNotUpdatedException;
import com.inyourhead.backend.utils.ImageBase64Converter;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;

/**
 * Created by aleksander on 15.04.17.
 */
@Service
@Profile("test")
public class EmbeddedDatabaseImagesInitializer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    DataRepository dataRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        //zmień zdjęcia dla sklepów
        SklepDTO sklep = new SklepDTO();

        byte[] biedronka = null, jogurt = null, jogurtDefault = null, zabka = null, kebab = null, maslo = null;

        try {
            biedronka = convertImageFromURLToByteArray("http://www.mama-bloguje.com/wp-content/uploads/2014/10/biedronka.jpg");
            jogurt = convertImageFromURLToByteArray("http://allenabial.pl/wp-content/uploads/2015/12/jogurtswiateczny_92d198799f73a2f83f0e24803203f600.jpg");
            jogurtDefault = convertImageFromURLToByteArray("http://www.jakkupowac.pl/vars/upload/fckeditor/image/jogurt/truskawka_mlekovita.jpg");
            zabka = convertImageFromURLToByteArray("http://pobierak.jeja.pl/images/4/8/7/139145_nowe-logo-zabki.jpg");
            kebab = convertImageFromURLToByteArray("http://www.kebabpoznan.pl/components/menu/photos/33.jpg");
            maslo = convertImageFromURLToByteArray("https://s-media-cache-ak0.pinimg.com/736x/8a/c8/21/8ac82121763a631823a64a1315df5707.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //ustaw zdjęcie testowych sklepów
        try {
            sklep.setZdjecie(ImageBase64Converter.encodeToString(biedronka));
            dataRepository.updateSelectedStore(1, 1, sklep);
            dataRepository.updateSelectedStore(1, 3, sklep);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (EntityNotUpdatedException e) {
            e.printStackTrace();
        }

        try {
            sklep.setZdjecie(ImageBase64Converter.encodeToString(zabka));
            dataRepository.updateSelectedStore(1, 2, sklep);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (EntityNotUpdatedException e) {
            e.printStackTrace();
        }

        //ustaw zdjecia domyslnych produktów

        DefaultProductDTO produkt = new DefaultProductDTO();
        produkt.setZdjecie(ImageBase64Converter.encodeToString(jogurtDefault));


        try {
            dataRepository.saveOrUpdateDefaultProduct(2, produkt);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        produkt.setZdjecie(ImageBase64Converter.encodeToString(kebab));
        try {
            dataRepository.saveOrUpdateDefaultProduct(3, produkt);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        produkt.setZdjecie(ImageBase64Converter.encodeToString(maslo));
        try {
            dataRepository.saveOrUpdateDefaultProduct(1, produkt);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        //ustaw zdjęcia produktów adminów

        ProduktDTO produktDTO = new ProduktDTO();

        produktDTO.setZdjecie(ImageBase64Converter.encodeToString(jogurt));

        try {
            dataRepository.updateSelectedProduct(1, 2, 2, produktDTO);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (EntityNotUpdatedException e) {
            e.printStackTrace();
        }

    }

    private byte[] convertImageFromURLToByteArray(String url) throws IOException {
        return IOUtils.toByteArray(new URL(url));
    }
}
