package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.interfaces.StoreFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import com.inyourhead.backend.utils.enums.UserRole;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 04.12.16.
 */
public class StoreFacadeImpl implements StoreFacade {

    private DataRepository repository;
    private Environment env;
    private ClaimsUtils utils;

    public StoreFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }

    @Override
    public SklepDTO findStoreByUserIdAndStoreId(ServletRequest request, Integer storeId) throws EntityNotFoundException, UserNotPermittedException {

        SklepDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.findStoreByUserIdAndStoreId(details.getUserId(), storeId);

        if (response == null)
            throw new EntityNotFoundException("Wybrany sklep nie istnieje, bądź nie jesteś jego zarządcą!");

        return response;
    }

    @Override
    public List<SklepDTO> getAllStories(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {

        List<SklepDTO> response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.findStoriesByUserId(details.getUserId());

        return response;

    }

    @Override
    public DeleteDetails deleteAllStoriesOfSelectedUser(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = new DeleteDetails(repository.deleteAllStoriesOfSelectedUser(details.getUserId()));

        if (response.getDeleteCount() == null) throw new EntityNotDeletedException("Nie usunieto zadnego sklepu!");

        return response;
    }

    @Override
    public DeleteDetails deleteSelectedStoryOfSelectedUser(ServletRequest request, Integer storeId) throws EntityNotDeletedException, UserNotPermittedException {

        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = new DeleteDetails(repository.deleteSelectedStoreOfUser(storeId, details.getUserId()));

        if (response.getDeleteCount() == null) throw new EntityNotDeletedException("Nie usunieto zadnego sklepu!");

        return response;
    }

    @Override
    public SklepDTO createNewStore(ServletRequest request, SklepDTO store) throws UserNotPermittedException, EntityNotCreatedException, EntityNotFoundException {

        SklepDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        if (store.getId_kat() == null) throw new EntityNotCreatedException("Id kategorii sklepu nie może być puste!");

        response = repository.createNewStore(details.getUserId(), store);

        if (response == null) throw new EntityNotCreatedException("Nie stworzono nowego sklepu!");

        return response;
    }

    @Override
    public SklepDTO updateSelectedStore(ServletRequest request, SklepDTO store, Integer storeId) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException {
        SklepDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.updateSelectedStore(details.getUserId(), storeId, store);

        if (response == null) throw new EntityNotUpdatedException("Nie udało się zaktualizować sklepu!");

        return response;
    }
}
