package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.HistoriaDTO;
import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EntityNotDeletedException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.interfaces.HistoryFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 25.12.16.
 */
public class HistoryFacadeImpl implements HistoryFacade {

    private final ClaimsUtils utils;
    private Environment env;
    private DataRepository repository;

    public HistoryFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }

    @Override
    public List<HistoriaDTO> getHistory(ServletRequest request) throws UserNotPermittedException {
        List<HistoriaDTO> response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.findUserHistoryByUserId(details.getUserId());

        return response;
    }

    @Override
    public DeleteDetails deleteHistory(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = new DeleteDetails(repository.deleteHistoryByUserId(details.getUserId()));

        return response;

    }

    @Override
    public HistoriaDTO saveSingleQuestion(ServletRequest request, ZapytanieDTO question) throws UserNotPermittedException, EntityNotFoundException {
        HistoriaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.saveSingleQuestion(details.getUserId(), question);

        return response;
    }
}
