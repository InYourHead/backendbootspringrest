package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.NewPasswordDTO;
import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.interfaces.UserFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.PasswordGenerator;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by aleksander on 08.12.16.
 */
public class UserFacadeImpl implements UserFacade {

    private Environment env;
    private DataRepository repository;
    private ClaimsUtils utils;
    private TokenRepository tokenRepository;

    public UserFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.tokenRepository = tokenRepository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }


    @Override
    public UzytkownikDTO getUserInfo(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException {
        UzytkownikDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.findUserById(details.getUserId());

        return response;
    }

    @Override
    public DeleteDetails deleteUserAccount(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = new DeleteDetails(repository.deleteUserById(details.getUserId()));

        return response;
    }

    @Override
    public UzytkownikDTO updateUserData(ServletRequest request, UzytkownikDTO user) throws UserNotPermittedException, EntityNotFoundException {
        UzytkownikDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.updateUser(details.getUserId(), user);

        return response;
    }

    @Override
    public List<TokenDTO> getTokens(ServletRequest request) throws UserNotPermittedException {
        List<TokenDTO> tempResponse;

        List<TokenDTO> response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        tempResponse = tokenRepository.findByUserId(details.getUserId());


        //ignoruj tokeny wystawione na potrzeby resetowania hasła
        response = tempResponse.stream().filter(token -> !token.getDevice_id().equals("RESET_PASSWORD")).collect(Collectors.toList());

        return response;
    }

    @Override
    public DeleteDetails deleteTokens(ServletRequest request) throws UserNotPermittedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = new DeleteDetails(tokenRepository.deleteByUserId(details.getUserId()).intValue());

        return response;
    }

    @Override
    public void changePassword(ServletRequest request, NewPasswordDTO newPassword) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, NotValidQueryException {

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (newPassword.getNewPassword() == null || newPassword.getNewPassword().length() < 8)
            throw new NotValidQueryException("Hało jest za krótkie!");

        String encryptedPassword = PasswordGenerator.generate(newPassword.getNewPassword());

        repository.changePasswordOfTheSelectedUser(details.getUserId(), encryptedPassword);

        if (utils.getUserDevice(request).equals("RESET_PASSWORD")) {
            tokenRepository.deleteByToken(details.getToken());
        }
    }
}
