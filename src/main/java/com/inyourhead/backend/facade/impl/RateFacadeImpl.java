package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.OcenaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.EntityNotUpdatedException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.interfaces.RateFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 06.04.17.
 */
public class RateFacadeImpl implements RateFacade {


    private DataRepository repository;
    private Environment env;
    private ClaimsUtils utils;

    public RateFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }

    @Override
    public OcenaDTO rateProduct(ServletRequest request, Integer productId, OcenaDTO rate) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, EmptyFieldsException {

        if (rate.getOcena() == null || rate.getOcena() < 1 || rate.getOcena() > 5)
            throw new EmptyFieldsException("Ocena powinna być z zakresu 1-5!");

        OcenaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.rateSelectedProduct(details.getUserId(), rate.getOcena().intValue(), productId);

        return response;
    }

    @Override
    public OcenaDTO getUserRateOfSelectedProduct(ServletRequest request, Integer productId) throws UserNotPermittedException {
        OcenaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        response = repository.getSelectedProductRate(details.getUserId(), productId);

        return response;
    }
}
