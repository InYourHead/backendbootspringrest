package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DefaultProductDTO;
import com.inyourhead.backend.data.dto.DistrictDTO;
import com.inyourhead.backend.data.dto.KategoriaDTO;
import com.inyourhead.backend.data.dto.WynikProduktObiektDTOImpl;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.facade.interfaces.CategoryFacade;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksander on 30.12.16.
 */
public class CategoryFacadeImpl implements CategoryFacade {

    private Environment env;
    private DataRepository repository;

    public CategoryFacadeImpl(DataRepository repository, Environment env) {
        this.repository = repository;
        this.env = env;
    }

    @Override
    public List<KategoriaDTO> getAllCategories() {
        List<KategoriaDTO> result = null;

        result = repository.getAllCategories();

        return result;
    }

    @Override
    public List<DistrictDTO> getAllProvinces() {
        ArrayList<DistrictDTO> result= new ArrayList<>();

        String provinces[]={
                "dolnośląskie",
                "kujawsko-pomorskie",
                "lubelskie",
                "lubuskie",
                "łódzkie",
                "małopolskie",
                "mazowieckie",
                "opolskie",
                "podkarpackie",
                "podlaskie",
                "pomorskie",
                "śląskie",
                "świętokrzyskie",
                "warmińsko-mazurskie",
                "wielkopolskie",
                "zachodniopomorskie"};

        for(int i=1; i<=provinces.length; i++){

            result.add(new DistrictDTO(i,provinces[i-1]));
        }

        return result;
    }

    @Override
    public List<DefaultProductDTO> getAllDefaultProducts() {
        List<DefaultProductDTO> result;

        result = repository.getAllDefaultProducts();

        return result;
    }

    @Override
    public DefaultProductDTO createDefaultProduct(DefaultProductDTO newDefaultProduct) throws EntityNotFoundException {
        DefaultProductDTO result;

        result = repository.createDefaultProduct(newDefaultProduct);

        return result;
    }

    @Override
    public List<WynikProduktObiektDTOImpl> getProductsOfSelectedStore(Integer storeId, Integer size) {
        List<WynikProduktObiektDTOImpl> result;

        result = repository.getProductsOfSelectedStore(storeId, size);

        return result;
    }

}
