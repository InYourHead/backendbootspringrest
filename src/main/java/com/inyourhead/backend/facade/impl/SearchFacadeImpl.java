package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.NotValidQueryException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.interfaces.HistoryFacade;
import com.inyourhead.backend.facade.interfaces.SearchFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.util.Comparator;
import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
public class SearchFacadeImpl implements SearchFacade {

    private DataRepository repository;
    private Environment env;
    private ClaimsUtils utils;
    private HistoryFacade historyFacade;


    private Integer defaultSize = 8;

    public SearchFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
        historyFacade = new HistoryFacadeImpl(repository, tokenRepository, env);
    }

    @Override
    public List<WynikDTO> search(ZapytanieDTO fraza, Integer ilosc, Boolean szukajWOpisie, ServletRequest request) throws NotValidQueryException, UserNotPermittedException, EntityNotFoundException {

        if (ilosc == null) ilosc = defaultSize;

        Boolean isAuthenticated = true;

        List<WynikDTO> response;

        if (fraza.getFraza() == null || fraza.getLat() == null || fraza.getLng() == null)
            throw new NotValidQueryException("Wyszukiwana fraza, bądź lokalizacja nie została podana!");

        try {
            utils.getUserDetails(request);
        } catch (UserNotPermittedException e) {
            isAuthenticated = false;
        }

        //zapisujemy historię wyszukiwania dla zalogowanego użytkownika
        if (isAuthenticated)
            historyFacade.saveSingleQuestion(request, fraza);

        //wyszukajmy w koncu!
        //w zależności od ustalonego parametru size zwrócmy do n wyników, bądź ustaloną liczbę!
        response = repository.getSearchResult(fraza, szukajWOpisie, ilosc);

        return response;
    }

    private List<WynikDTO> getSortedListOfResultsByDistance(Double clientX, Double clientY, List<WynikDTO> unsortedResult) {

        unsortedResult.sort(Comparator.comparing(o -> (Math.hypot(o.getX() - clientX, o.getY() - clientY))));

        return unsortedResult;

    }
}
