package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.facade.interfaces.LoginFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.utils.TokenGenerator;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.security.Principal;

/**
 * Created by aleksander on 29.11.16.
 */
public class LoginFacadeImpl implements LoginFacade {


    private TokenRepository tokenRepository;

    private ClaimsUtils claimsUtils;

    private TokenGenerator tokenGenerator;

    public LoginFacadeImpl(DataRepository dataRepository, TokenRepository tokenRepository, Environment env) {
        this.tokenRepository = tokenRepository;
        this.claimsUtils = new ClaimsUtils(env, tokenRepository, dataRepository);
        this.tokenGenerator = new TokenGenerator(dataRepository, tokenRepository, env);
    }

    @Override
    public Token getTokenForLoggedUser(Principal principal, ServletRequest request) {

        Token authToken = tokenGenerator.createToken(principal.getName(), request);

        return authToken;
    }

    @Override
    public TokenDTO logoutUser(ServletRequest request) throws UserNotPermittedException {

        TokenDTO response = null;
        UserAuthenticationDetails details = claimsUtils.getUserDetails(request);

        response = (tokenRepository.findByToken(details.getToken())).toTokenDTO();

        tokenRepository.deleteByToken(details.getToken());

        return response;
    }
}
