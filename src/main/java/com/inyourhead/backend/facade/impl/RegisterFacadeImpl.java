package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.data.entities.Uzytkownik;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.email.EmailService;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityExistsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.facade.interfaces.RegisterFacade;
import com.inyourhead.backend.utils.PasswordGenerator;
import com.inyourhead.backend.utils.Token;
import com.inyourhead.backend.utils.TokenGenerator;
import org.springframework.core.env.Environment;

/**
 * Created by aleksander on 30.11.16.
 */

public class RegisterFacadeImpl implements RegisterFacade {

    private DataRepository repository;
    private EmailService emailService;
    private TokenGenerator tokenGenerator;

    public RegisterFacadeImpl(DataRepository repository, EmailService emailService, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.emailService = emailService;
        this.tokenGenerator = new TokenGenerator(repository, tokenRepository, env);
    }


    @Override
    public UzytkownikDTO createNewUser(UzytkownikDTO user) throws EmptyFieldsException, EntityExistsException {
        if (user.getEmail() == null || user.getLogin() == null || user.getHaslo() == null)
            throw new EmptyFieldsException("Email, login, ani hasło nie mogą być puste!");

        if(!user.getEmail().contains("@")) throw new EmptyFieldsException("Niepoprawny email!");

        if(!(user.getHaslo().length()>=8)) throw new EmptyFieldsException("Hasło musi być długości większej bądź równej 8!");

        String hashPassword = PasswordGenerator.generate(user.getHaslo());

        //hashowanie hasła
        user.setHaslo(hashPassword);

        Uzytkownik mappedUser = user.toUzytkownik();
        mappedUser.setIdUser(0);

        Uzytkownik savedUser = repository.save(mappedUser);

        if (savedUser == null) throw new EntityExistsException("Uzytkownik o podanym adresie, badz emailu istnieje!");

        UzytkownikDTO createdUser = new UzytkownikDTO(repository.getOne(repository.findUserIdByLogin(user.getLogin())));
        //a po chuj im haslo?
        createdUser.setHaslo(null);

        //send email to new user
        emailService.sendConfirmationEmail(createdUser);

        return createdUser;
    }

    @Override
    public void sendResetUserPasswordEmailUsingLoginOrEmail(String login) throws EntityNotFoundException {

        Token tempToken;

        Uzytkownik tempUser = repository.findUserByLoginOrEmail(login);

        if (tempUser != null) {

            tempToken = tokenGenerator.createToken(tempUser.getLogin(), null);
            emailService.sendRemindPasswordEmail(tempUser.toUzytkownikDTO(), tempToken.getToken());

        } else throw new EntityNotFoundException("Użytkownik o podanym loginie, bądź adresie email nie istnieje!");
    }
}
