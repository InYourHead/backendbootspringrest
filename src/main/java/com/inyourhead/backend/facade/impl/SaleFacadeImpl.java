package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.PromocjaDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.interfaces.SaleFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import com.inyourhead.backend.utils.enums.UserRole;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 28.12.16.
 */
public class SaleFacadeImpl implements SaleFacade {

    private DataRepository repository;
    private Environment env;
    private ClaimsUtils utils;

    public SaleFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }

    @Override
    public PromocjaDTO getSale(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotFoundException {

        PromocjaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.getSaleForSelectedProduct(details.getUserId(), storeId, productId);

        return response;
    }

    @Override
    public DeleteDetails deleteSale(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = new DeleteDetails(repository.deleteSaleForSelectedProduct(details.getUserId(), storeId, productId));

        return response;

    }

    @Override
    public PromocjaDTO updateSale(ServletRequest request, Integer storeId, Integer productId, PromocjaDTO sale) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException {

        PromocjaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.updateSelectedSale(details.getUserId(), storeId, productId, sale);

        return response;
    }

    @Override
    public PromocjaDTO createSale(ServletRequest request, Integer storeId, Integer productId, PromocjaDTO sale) throws UserNotPermittedException, EntityExistsException, EntityNotFoundException, EntityNotCreatedException {

        PromocjaDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.createNewSale(details.getUserId(), storeId, productId, sale);

        return response;
    }
}
