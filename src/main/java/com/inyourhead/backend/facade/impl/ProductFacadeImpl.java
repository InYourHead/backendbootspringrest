package com.inyourhead.backend.facade.impl;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.data.repositories.DataRepository;
import com.inyourhead.backend.data.repositories.token.TokenRepository;
import com.inyourhead.backend.exceptions.*;
import com.inyourhead.backend.facade.interfaces.ProductFacade;
import com.inyourhead.backend.utils.ClaimsUtils;
import com.inyourhead.backend.utils.UserAuthenticationDetails;
import com.inyourhead.backend.utils.enums.UserRole;
import org.springframework.core.env.Environment;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 26.12.16.
 */
public class ProductFacadeImpl implements ProductFacade {


    private DataRepository repository;
    private Environment env;
    private ClaimsUtils utils;

    public ProductFacadeImpl(DataRepository repository, TokenRepository tokenRepository, Environment env) {
        this.repository = repository;
        this.env = env;
        this.utils = new ClaimsUtils(env, tokenRepository, repository);
    }


    @Override
    public List<ProduktDTO> getAllProducts(ServletRequest request, Integer storeId) throws UserNotPermittedException, EntityNotFoundException {

        List<ProduktDTO> response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.findProductsByUserIdAndStoreId(details.getUserId(), storeId);

        return response;
    }

    @Override
    public DeleteDetails deleteAllProductsOfSelectedStore(ServletRequest request, Integer storeId) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = new DeleteDetails(repository.deleteProductsByUserIdAndStoreId(details.getUserId(), storeId));

        return response;
    }

    @Override
    public ProduktDTO addNewProductToSelectedStore(ServletRequest request, Integer storeId, ProduktDTO product) throws UserNotPermittedException, EntityNotCreatedException, EntityNotFoundException {
        ProduktDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.createNewProduct(details.getUserId(), storeId, product);

        if (response == null)
            throw new EntityNotCreatedException("Nie udało się utworzyć produktu, sprawdź wymagane pola!");

        return response;
    }

    @Override
    public ProduktDTO getSelectedProduct(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException {
        ProduktDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.getSelectedProduct(details.getUserId(), storeId, productId);

        return response;
    }

    @Override
    public DeleteDetails deleteSelectedProduct(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotDeletedException {
        DeleteDetails response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = new DeleteDetails(repository.deleteSelectedProduct(details.getUserId(), storeId, productId));

        return response;

    }

    @Override
    public ProduktDTO updateSelectedProduct(ServletRequest request, Integer storeId, Integer productId, ProduktDTO product) throws UserNotPermittedException, EntityNotFoundException, EntityNotUpdatedException {
        ProduktDTO response;

        UserAuthenticationDetails details = utils.getUserDetails(request);

        if (!details.getAccountType().equals(UserRole.ADMIN))
            throw new UserNotPermittedException("Nie jestes adminem!");

        response = repository.updateSelectedProduct(details.getUserId(), storeId, productId, product);

        return response;
    }
}
