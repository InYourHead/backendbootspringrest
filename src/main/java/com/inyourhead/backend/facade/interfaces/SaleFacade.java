package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.PromocjaDTO;
import com.inyourhead.backend.exceptions.*;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 28.12.16.
 */
public interface SaleFacade {
    PromocjaDTO getSale(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotFoundException;

    DeleteDetails deleteSale(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotDeletedException;

    PromocjaDTO updateSale(ServletRequest request, Integer storeId, Integer productId, PromocjaDTO sale) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException;

    PromocjaDTO createSale(ServletRequest request, Integer storeId, Integer productId, PromocjaDTO sale) throws UserNotPermittedException, EntityExistsException, EntityNotFoundException, EntityNotCreatedException;
}
