package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.SklepDTO;
import com.inyourhead.backend.exceptions.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
public interface StoreFacade {


    SklepDTO findStoreByUserIdAndStoreId(ServletRequest request, Integer storeId) throws UserNotPermittedException, EntityNotFoundException;

    List<SklepDTO> getAllStories(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException;

    DeleteDetails deleteAllStoriesOfSelectedUser(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException;

    DeleteDetails deleteSelectedStoryOfSelectedUser(ServletRequest request, Integer storeId) throws EntityNotDeletedException, UserNotPermittedException;

    SklepDTO createNewStore(ServletRequest request, SklepDTO store) throws UserNotPermittedException, EntityNotCreatedException, EntityNotFoundException;

    SklepDTO updateSelectedStore(ServletRequest request, SklepDTO store, Integer storeId) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException;
}
