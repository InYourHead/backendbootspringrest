package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityExistsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;

/**
 * Created by aleksander on 30.11.16.
 */
public interface RegisterFacade {

    UzytkownikDTO createNewUser(UzytkownikDTO user) throws EmptyFieldsException, EntityExistsException;

    void sendResetUserPasswordEmailUsingLoginOrEmail(String login) throws EntityNotFoundException;
}
