package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.HistoriaDTO;
import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.exceptions.EntityNotDeletedException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 25.12.16.
 */
public interface HistoryFacade {
    List<HistoriaDTO> getHistory(ServletRequest request) throws UserNotPermittedException;

    DeleteDetails deleteHistory(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException;

    HistoriaDTO saveSingleQuestion(ServletRequest request, ZapytanieDTO question) throws UserNotPermittedException, EntityNotFoundException;
}
