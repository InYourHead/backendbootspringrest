package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.NewPasswordDTO;
import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.data.dto.UzytkownikDTO;
import com.inyourhead.backend.exceptions.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 08.12.16.
 */
public interface UserFacade {


    UzytkownikDTO getUserInfo(ServletRequest request) throws UserNotPermittedException, EntityNotFoundException;

    DeleteDetails deleteUserAccount(ServletRequest request) throws UserNotPermittedException, EntityNotDeletedException;

    UzytkownikDTO updateUserData(ServletRequest request, UzytkownikDTO user) throws UserNotPermittedException, EntityNotFoundException;

    List<TokenDTO> getTokens(ServletRequest request) throws UserNotPermittedException;

    DeleteDetails deleteTokens(ServletRequest request) throws UserNotPermittedException;

    void changePassword(ServletRequest request, NewPasswordDTO newPassword) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, NotValidQueryException;
}
