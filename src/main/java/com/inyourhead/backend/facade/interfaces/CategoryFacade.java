package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DefaultProductDTO;
import com.inyourhead.backend.data.dto.DistrictDTO;
import com.inyourhead.backend.data.dto.KategoriaDTO;
import com.inyourhead.backend.data.dto.WynikProduktObiektDTOImpl;
import com.inyourhead.backend.exceptions.EntityNotFoundException;

import java.util.List;

/**
 * Created by aleksander on 30.12.16.
 */
public interface CategoryFacade {
    List<KategoriaDTO> getAllCategories();

    List<DistrictDTO> getAllProvinces();

    List<DefaultProductDTO> getAllDefaultProducts();

    DefaultProductDTO createDefaultProduct(DefaultProductDTO newDefaultProduct) throws EntityNotFoundException;

    List<WynikProduktObiektDTOImpl> getProductsOfSelectedStore(Integer storeId, Integer size);
}
