package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.DeleteDetails;
import com.inyourhead.backend.data.dto.ProduktDTO;
import com.inyourhead.backend.exceptions.*;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 26.12.16.
 */
public interface ProductFacade {
    List<ProduktDTO> getAllProducts(ServletRequest request, Integer storeId) throws UserNotPermittedException, EntityNotFoundException;

    DeleteDetails deleteAllProductsOfSelectedStore(ServletRequest request, Integer storeId) throws UserNotPermittedException, EntityNotDeletedException;

    ProduktDTO addNewProductToSelectedStore(ServletRequest request, Integer storeId, ProduktDTO product) throws UserNotPermittedException, EntityNotCreatedException, EntityNotFoundException;

    ProduktDTO getSelectedProduct(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException;

    DeleteDetails deleteSelectedProduct(ServletRequest request, Integer storeId, Integer productId) throws UserNotPermittedException, EntityNotDeletedException;

    ProduktDTO updateSelectedProduct(ServletRequest request, Integer storeId, Integer productId, ProduktDTO product) throws UserNotPermittedException, EntityNotFoundException, EntityNotUpdatedException;
}
