package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.TokenDTO;
import com.inyourhead.backend.exceptions.UserNotPermittedException;
import com.inyourhead.backend.utils.Token;

import javax.servlet.ServletRequest;
import java.security.Principal;

/**
 * Created by aleksander on 29.11.16.
 */
public interface LoginFacade {

    Token getTokenForLoggedUser(Principal principal, ServletRequest request);

    TokenDTO logoutUser(ServletRequest request) throws UserNotPermittedException;
}
