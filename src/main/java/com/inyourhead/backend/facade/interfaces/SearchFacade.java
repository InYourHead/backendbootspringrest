package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.ZapytanieDTO;
import com.inyourhead.backend.data.dto.interfaces.WynikDTO;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.NotValidQueryException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;

import javax.servlet.ServletRequest;
import java.util.List;

/**
 * Created by aleksander on 28.11.16.
 */
public interface SearchFacade {

    List<WynikDTO> search(ZapytanieDTO fraza, Integer ilosc, Boolean szukajWOpisie, ServletRequest request) throws NotValidQueryException, UserNotPermittedException, EntityNotFoundException;
}
