package com.inyourhead.backend.facade.interfaces;

import com.inyourhead.backend.data.dto.OcenaDTO;
import com.inyourhead.backend.exceptions.EmptyFieldsException;
import com.inyourhead.backend.exceptions.EntityNotFoundException;
import com.inyourhead.backend.exceptions.EntityNotUpdatedException;
import com.inyourhead.backend.exceptions.UserNotPermittedException;

import javax.servlet.ServletRequest;

/**
 * Created by aleksander on 06.04.17.
 */
public interface RateFacade {

    OcenaDTO rateProduct(ServletRequest request, Integer productId, OcenaDTO rate) throws UserNotPermittedException, EntityNotUpdatedException, EntityNotFoundException, EmptyFieldsException;

    OcenaDTO getUserRateOfSelectedProduct(ServletRequest request, Integer productId) throws UserNotPermittedException;
}
