package com.inyourhead.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SuppressWarnings("javadoc")
@SpringBootApplication
@EnableScheduling
public class BackendApplication extends SpringBootServletInitializer
{

	public static void main(String[] args)
	{
		SpringApplication.run(BackendApplication.class, args);
	}


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(BackendApplication.class);
	}
}
