package com.inyourhead.backend.email;

import com.inyourhead.backend.data.dto.UzytkownikDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;


/**
 * Created by aleksander on 09.04.17.
 */
@Service
public class EmailService {

    private JavaMailSender mailSender;
    private TemplateEngine templateEngine;
    private Environment env;

    private String SENDER_EMAIL;

    public EmailService() {
        super();
    }

    @Autowired
    public EmailService(JavaMailSender mailSender, TemplateEngine templateEngine, Environment env) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.env = env;
    }

    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Autowired
    public void setTemplateEngine(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Autowired
    public void setEnv(Environment env) {
        this.env = env;
        SENDER_EMAIL = this.env.getProperty("email.address");
    }

    public void sendConfirmationEmail(final UzytkownikDTO user) {
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo(user.getEmail());
            message.setFrom(SENDER_EMAIL);
            message.setSubject("Rejestracja w serwisie gdzie-znajde.com.");

            org.thymeleaf.context.Context context = new org.thymeleaf.context.Context();

            context.setVariable("login", user.getLogin());
            context.setVariable("email", user.getEmail());

            String text = templateEngine.process("registration-email", context);

            message.setText(text, true);

        };
        this.mailSender.send(preparator);
    }

    public void sendRemindPasswordEmail(final UzytkownikDTO user, String forgetPasswordLink) {
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo(user.getEmail());
            message.setFrom(SENDER_EMAIL);
            message.setSubject("Przypomnienie hasła w serwisie gdzie-znajde.com.");

            org.thymeleaf.context.Context context = new org.thymeleaf.context.Context();

            context.setVariable("login", user.getLogin());
            context.setVariable("forgetPasswordLink", forgetPasswordLink);

            String text = templateEngine.process("forgot-password", context);

            message.setText(text, true);
        };
        this.mailSender.send(preparator);
    }
}
