package com.inyourhead.backend.exceptions;

/**
 * Created by aleksander on 04.12.16.
 */
public class UserNotPermittedException extends Exception {
    public UserNotPermittedException() {
    }

    public UserNotPermittedException(String message) {
        super(message);
    }

    public UserNotPermittedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotPermittedException(Throwable cause) {
        super(cause);
    }

    public UserNotPermittedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
