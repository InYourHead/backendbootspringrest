package com.inyourhead.backend.exceptions;

/**
 * Created by aleksander on 07.12.16.
 */
public class NotValidQueryException extends Exception{
    public NotValidQueryException() {
    }

    public NotValidQueryException(String message) {
        super(message);
    }

    public NotValidQueryException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotValidQueryException(Throwable cause) {
        super(cause);
    }

    public NotValidQueryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
