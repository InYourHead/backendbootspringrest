package com.inyourhead.backend.exceptions;

/**
 * Created by aleksander on 04.12.16.
 */
public class EntityNotCreatedException extends Exception {


    public EntityNotCreatedException() {
        super();
    }

    public EntityNotCreatedException(String message) {
        super(message);
    }

    public EntityNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotCreatedException(Throwable cause) {
        super(cause);
    }

    protected EntityNotCreatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
