package com.inyourhead.backend.exceptions;

/**
 * Created by aleksander on 04.12.16.
 */
public class EntityNotUpdatedException extends Exception {
    public EntityNotUpdatedException() {
    }

    public EntityNotUpdatedException(String message) {
        super(message);
    }

    public EntityNotUpdatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotUpdatedException(Throwable cause) {
        super(cause);
    }

    public EntityNotUpdatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
