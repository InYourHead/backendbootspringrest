package com.inyourhead.backend.exceptions;

/**
 * Created by aleksander on 04.12.16.
 */
public class EntityNotDeletedException extends Exception {
    public EntityNotDeletedException() {
        super();
    }

    public EntityNotDeletedException(String message) {
        super(message);
    }

    public EntityNotDeletedException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotDeletedException(Throwable cause) {
        super(cause);
    }

    protected EntityNotDeletedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
