SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS token CASCADE;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE token
(
  token           VARCHAR(300),
  singing_key     VARCHAR(256),
  device_id       VARCHAR(500),
  create_date     DATE,
  user_id         INTEGER,
  PRIMARY KEY (token)

);