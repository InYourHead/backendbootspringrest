
INSERT INTO `kategoria` (`nazwa`) VALUES ('Sklep');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Usługa');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Moda i uroda');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Dom i zdrowie');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Dziecko');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Kultura i rozrywka');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Sport i wypoczynek');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Motoryzacja');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Kolekcje i sztuka');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Firma');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Spozywczy');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Kebab');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Pizza');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Lody');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Artykuly budowlane');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Fotografia');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Drogeria');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Perfumy');
INSERT INTO `kategoria` (`nazwa`) VALUES ('Turystyka');

INSERT INTO `uzytkownik` (`typ`, `imie`, `nazwisko`, `email`, `telefon`, `adres_ulica`, `adres_nr_domu`, `adres_kod`, `adres_miasto`, `adres_woj`, `haslo`, `login`)
VALUES ('1', 'Olaf', 'Majer', 'nieistniejacyMAilFalo92@mat.umk.pl', '123456789', 'Rakowicza 1A', '13', '87-100', 'Torun',
             'Kujawsko-Pomorskie', '$2a$10$NIrq7UevEzkaucpIPtN2meBStMV8rMSRni6l5MrgLTQfCy6UGofqO', 'admin');
INSERT INTO `uzytkownik` (`typ`, `imie`, `nazwisko`, `email`, `telefon`, `adres_ulica`, `adres_nr_domu`, `adres_kod`, `adres_miasto`, `adres_woj`, `haslo`, `login`)
VALUES ('0', 'Aleksander', 'Burzec', 'aburzec@localhost', '135792468', 'Szeroka 9', '2', '87-100', 'Torun',
             'Kujawsko-Pomorskie', '$2a$10$wqGvaqzSioUfzBbn8J841uZV4JlkmZLgdeSIADgOhl56gq6Hau7T2', 'user');


INSERT INTO `sklep` (`nazwa`, `adres_ulica`, `adres_nr_domu`, `adres_kod`, `adres_miasto`, `adres_woj`, `zdjecie`, `id_user`, `facebook`, `www`, `wspol_x`, `wspol_y`)
VALUES ('Biedronka', 'Szeroka', '1', '87-100', 'Torun', 'Kujawsko-Pomorskie', NULL, '1',
                     'https://www.facebook.com/pages/Biedronka/1510320899234907?fref=ts', 'http://www.biedronka.pl/pl',
                     '18.608776', '53.010675');
INSERT INTO `sklep` (`nazwa`, `adres_ulica`, `adres_nr_domu`, `adres_kod`, `adres_miasto`, `adres_woj`, `zdjecie`, `id_user`, `facebook`, `www`, `wspol_x`, `wspol_y`)
VALUES ('Żabka', 'Male Garbary 22', '9', '87-100', 'Torun', 'Kujawsko-Pomorskie', NULL, '1',
                 'https://www.facebook.com/zabkapolska/?fref=ts', 'http://zabka.pl/pl', '18.608260', '53.011963');

INSERT INTO `sklep` (`nazwa`, `adres_ulica`, `adres_nr_domu`, `adres_kod`, `adres_miasto`, `adres_woj`, `zdjecie`, `id_user`, `facebook`, `www`, `wspol_x`, `wspol_y`)
VALUES ('Biedronka', 'Długa', '12', '87-100', 'Torun', 'Kujawsko-Pomorskie', NULL, '1',
                     'https://www.facebook.com/pages/Biedronka/1510320899234907?fref=ts', 'http://www.biedronka.pl/pl',
                     '18.62776', '53.030675');

INSERT INTO `kat_sklep` (`id_sklep`, `id_kat`) VALUES ('1', '11');
INSERT INTO `kat_sklep` (`id_sklep`, `id_kat`) VALUES ('2', '13');
INSERT INTO `kat_sklep` (`id_sklep`, `id_kat`) VALUES ('3', '11');

INSERT INTO `produkt` (`nazwa`, `opis`, `zdjecie`,`id_kat`) VALUES ('Maslo', 'Opis domyslny masłą',NULL, '11');
INSERT INTO `produkt` (`nazwa`, `opis`, `zdjecie`,`id_kat`) VALUES ('Jogurt','Opis domyślny: To jest jogurt',NULL,  '9');
INSERT INTO `produkt` (`nazwa`, `opis`, `zdjecie`,`id_kat`) VALUES ('Kebab','Opis domyślny: To jest kebab',NULL,  '12');

INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('3','2','Kebab drobiowy z żabki','7.99',NULL,NULL );
INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('1','2','Masło orzechowe','12.99',NULL,NULL );
INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('2','2','Jogurt','1.99','Opis jogurtu admina',NULL );

INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('3','1','Kebab drobiowy z biedronki','3.99','Opis admina biedronki',NULL );
INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('1','1','Masło orzechowe z biedronki','12.99',NULL,NULL );
INSERT INTO `prod_szczeg` (`id_prod`, `id_sklep`,`nazwa`, `cena`,`opis`, `zdjecie`) VALUES ('2','1',NULL,'1.99','Opis jogurtu admina z biedronki',NULL );

INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('1','mleko','1', null, null, null);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('5','igla','1',4.99,null, 4);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('2','kebab ostry','1', 7.99, 15.99, null);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('1','maslo','2', null, null, null);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('2','zeszyt','2', null, null, 4);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('3','buty','2', null, null, null);
INSERT INTO `historia` (`nr`,`fraza`,`id_user`, `cena_min`, `cena_max`, `id_kat`) VALUES ('4','zapiekanka','2', null, null, null);

INSERT INTO `promocja` (`id_prod`, `cena`, `id_sklep`) VALUES ('4', '10.99', '1');
INSERT INTO `promocja` (`id_prod`, `cena`, `id_sklep`) VALUES ('3', '29.5','1');

INSERT INTO `oceny_prod` (`id_prod_szczeg`, `id_user`, `ocena`) VALUES( '1', '1', '5');
INSERT INTO `oceny_prod` (`id_prod_szczeg`, `id_user`, `ocena`) VALUES( '1', '2', '4');