
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS historia, kategoria, produkt, sklep, uzytkownik, promocja, prod_szczeg, oceny_prod, kat_sklep CASCADE;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE uzytkownik
(
  id_user       INTEGER NOT NULL AUTO_INCREMENT,
  typ           INTEGER ,
  imie          VARCHAR (20) ,
  nazwisko      VARCHAR (30) ,
  email         VARCHAR (50) NOT NULL UNIQUE,
  telefon       VARCHAR (13) ,
  adres_ulica   VARCHAR (20) ,
  adres_nr_domu VARCHAR (4) ,
  adres_kod     VARCHAR (6) ,
  adres_miasto  VARCHAR (20) ,
  adres_woj     VARCHAR (40) ,
  haslo         VARCHAR (300) NOT NULL ,
  login         VARCHAR (30) NOT NULL UNIQUE ,
  PRIMARY KEY (id_user)
) ;

CREATE TABLE historia
(
  nr      INTEGER NOT NULL AUTO_INCREMENT,
  fraza   VARCHAR (50) ,
  id_user INTEGER NOT NULL,
  cena_min DOUBLE,
  cena_max DOUBLE ,
  id_kat   INTEGER,
  data     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT hist_uzyt_fk1235 FOREIGN KEY (id_user) REFERENCES uzytkownik (id_user)
    ON DELETE CASCADE
) ;

CREATE TABLE sklep
(
  id_sklep      INTEGER NOT NULL AUTO_INCREMENT,
  nazwa         VARCHAR (100) ,
  facebook      VARCHAR(200),
  www           VARCHAR(100),
  adres_ulica   VARCHAR (20) ,
  adres_nr_domu VARCHAR (4) ,
  adres_kod     VARCHAR (6) ,
  adres_miasto  VARCHAR (20) ,
  adres_woj     VARCHAR (40) ,
  zdjecie       LONGBLOB,
  id_user       INTEGER NOT NULL,
  wspol_x       DOUBLE,
  wspol_y       DOUBLE,
  PRIMARY KEY (id_sklep),
  CONSTRAINT sklep_uzyt_fk2134 FOREIGN KEY (id_user) REFERENCES uzytkownik (id_user)
    ON DELETE CASCADE
) ;

CREATE TABLE kategoria
(
  id_kat          INTEGER NOT NULL AUTO_INCREMENT ,
  nazwa           VARCHAR (30),
  PRIMARY KEY (id_kat)
) ;

CREATE TABLE produkt
(
  id_prod INTEGER NOT NULL AUTO_INCREMENT,
  nazwa   VARCHAR (100) ,
  opis    VARCHAR (1000) ,
  zdjecie LONGBLOB,
  id_kat  INTEGER NOT NULL,
  PRIMARY KEY (id_prod),
  CONSTRAINT prod_kat_fk4213 FOREIGN KEY (id_kat) REFERENCES kategoria (id_kat)
    ON DELETE CASCADE
);

CREATE TABLE prod_szczeg
(
  id_prod_szczeg INTEGER NOT NULL AUTO_INCREMENT,
  id_prod        INTEGER NOT NULL,
  nazwa          VARCHAR(100),
  opis           VARCHAR(1000),
  cena           DOUBLE (2),
  zdjecie        LONGBLOB,
  id_sklep       INTEGER NOT NULL,
  PRIMARY KEY (id_prod_szczeg),
  CONSTRAINT prod_sklep_fk15 FOREIGN KEY (id_sklep) REFERENCES sklep (id_sklep)
    ON DELETE CASCADE,
  CONSTRAINT prod_fk12 FOREIGN KEY (id_prod) REFERENCES produkt (id_prod)
    ON DELETE CASCADE
);

CREATE TABLE kat_sklep
(
  id_kat_sklep INTEGER AUTO_INCREMENT,
  id_sklep     INTEGER NOT NULL,
  id_kat       INTEGER NOT NULL,
  PRIMARY KEY (id_kat_sklep),
  CONSTRAINT sklep_fk13 FOREIGN KEY (id_sklep) REFERENCES sklep (id_sklep)
    ON DELETE CASCADE,
  CONSTRAINT kat_fk1 FOREIGN KEY (id_kat) REFERENCES kategoria (id_kat)
    ON DELETE CASCADE
);

CREATE TABLE oceny_prod
(
  id_prod_szczeg INTEGER NOT NULL,
  id_user        INTEGER NOT NULL,
  ocena          INTEGER NOT NULL,
  PRIMARY KEY (id_prod_szczeg, id_user),
  CONSTRAINT produkt_fk12 FOREIGN KEY (id_prod_szczeg) REFERENCES prod_szczeg (id_prod_szczeg)
    ON DELETE CASCADE,
  CONSTRAINT user_fk312 FOREIGN KEY (id_user) REFERENCES uzytkownik (id_user)
    ON DELETE CASCADE
);

CREATE TABLE promocja
(
  id_prom  INTEGER NOT NULL AUTO_INCREMENT,
  id_prod  INTEGER NOT NULL,
  cena     DOUBLE (2),
  id_sklep INTEGER NOT NULL,
  PRIMARY KEY (id_prom),
  CONSTRAINT prom_prod_fk1234 FOREIGN KEY (id_prod) REFERENCES prod_szczeg (id_prod_szczeg)
    ON DELETE CASCADE,
  CONSTRAINT sklep_fk12343 FOREIGN KEY (id_sklep) REFERENCES sklep (id_sklep)
    ON DELETE CASCADE
);



